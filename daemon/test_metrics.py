#!/usr/bin/env python3


import time

from LsrId    import LsrId
from LsrHost  import LsrHost
from LsrCpu   import LsrCpu
from LsrMem   import LsrMem
from LsrGpu   import LsrGpu
from LsrDisk  import LsrDisk
from LsrNet   import LsrNet
from LsrProc  import LsrProc
from LsrClasses import *


# Test Machine -- please update values if running on a    # 
# different machine by reading the individual files       #
# defined in the LSR Software Design Document             #

#---------------------------------------------------------#
#                 Machine Indentifier Tests               #
#---------------------------------------------------------#
lsrid = LsrId()
#Individual Method checks, comparing outputs to file content
def test_id():
  assert lsrid.getSerial() == "2UA00317NK"
  assert lsrid.getManufacturer() == "Hewlett-Packard"
  assert lsrid.getName() == "HP Z800 Workstation"
  assert lsrid.getModel() == " "

  #Checks if the ID is formatted correctly into an ID class
  assert isinstance(lsrid.Id, Id) == True


#---------------------------------------------------------#
#                         Host Tests                      #
#---------------------------------------------------------#
lsrhost = LsrHost()
#Individual Method checks, comparing outputs to file content
def test_host():
  assert lsrhost.getHostname() == "jboffice.fishkill.ibm.com"
  assert lsrhost.getBiosVersion() == "786G5 v01.18"
  assert lsrhost.getBiosDate() == "12/08/2009"
  assert lsrhost.getOsRelease() == "Red Hat Enterprise Linux Workstation release 7.4 (Maipo)"
  assert lsrhost.getOsKernel() == "3.10.0-693.5.2.el7.x86_64"
  assert lsrhost.getOsArch() == "x86_64"
  assert lsrhost.getUptime() == "2021-02-15 11:44:18"

  #Checks if the Host is formatted correctly into a Host class
  assert isinstance(lsrhost.host, Host) == True


#---------------------------------------------------------#
#                         CPU Tests                       #
#---------------------------------------------------------#
lsrcpu = LsrCpu()
processors = lsrcpu.getCpuInfo()

#Check if the a dictionary was returned with each processor
def test_processor_keys():
  assert isinstance(processors, dict) == True

#Run utilization once to poluate initial active settings
lsrcpu.getUtilization()

#Loop through, testing each processor's info
def test_processor_static_info():
  for key, processor in processors.items():
    #Check if the processor key is a convertable to number
    assert isinstance(key, int) == True
    #Check if processor is formatted into the Processeor class from LSRClasses
    assert isinstance(processor, Processor) == True
    #Check if processor and core IDs is a number
    assert isinstance(processor.processor, int) == True
    assert int(processor.coreId) >= 0 
    assert int(processor.coreId) < 4
    #Check some defined attributes
    assert processor.vendor == "GenuineIntel"
    assert processor.model == "Intel(R) Xeon(R) CPU           E5540  @ 2.53GHz"
    assert processor.currentSpeed == "2533.430"
    assert processor.arch == "x64"
    #Check rated speed from dmesg 
    assert processor.ratedSpeed == "2533.430"

    #Check values were initialized
    assert processor.idle >= 0
    assert processor.total >= 0
    
#Run again to get time comparsion, wait 1 second for values to update
time.sleep(1)
lsrcpu.getUtilization()

#Loop through, testing each processor's info
def test_processor_active_info():
  for key, processor in processors.items():
    assert processor.idle >= 0
    assert processor.total >= 0
    assert processor.utilization >= 0


#---------------------------------------------------------#
#                     Memory Tests                        #
#---------------------------------------------------------#
lsrmem = LsrMem()

#Retrieve information about memory configuration
slots = lsrmem.getMemorySlotInfo()

#Check that each slot was successfully converted into a MemorySlot object
def test_memory_static_info():
  for slot in slots:
    assert isinstance(slot, MemorySlot) == True

#Check the active memory values
lsrmem.getMemInfo()

#Check values are reasonable
def test_memory_active_info():
  assert lsrmem.total > 0
  assert lsrmem.available >= 0
  assert lsrmem.cached >= 0
  assert lsrmem.swap >= 0


#---------------------------------------------------------#
#                      GPU Tests                          #
#---------------------------------------------------------#
lsrgpu = LsrGpu()

gpus = lsrgpu.getGpuInfo()

#Check that each GPU was successfully converted into a GPU object
def test_gpu_info():
  for gpu in gpus:
    assert isinstance(gpu, Gpu) == True
    assert gpu.name == "NVIDIA Corporation G96GL [Quadro FX 380] [10de:0658] (rev a1)"
    assert gpu.make == "NVIDIA"
    assert gpu.driver == "nouveau"
    

#---------------------------------------------------------#
#                      Disk Tests                         #
#---------------------------------------------------------#
lsrdisk = LsrDisk()

#Grab parition data for the default system disk /dev/sda
diskname = '/dev/sda'
total, partitions = lsrdisk.getPartitions(diskname)

#Check the calculation for total disk space from partitions is at least >1 mB
def test_disk_total_calculated():
  assert total > 0 

#Check partitions for valid entries
def test_partition_info():
  for key, partition in partitions.items():
    #Check the key identifier is the string for the partition path /dev/sdaX
    assert isinstance(key, str) == True
    #Verify the partition is part of the name diskname (/dev/sda in /dev/sda1)
    assert diskname in key

    #Check only ext and logical volumes were added, fail if one or the other not found
    try:
        assert "LVM" in partition.diskType
    except Exception as e:
        assert "ext" in partition.diskType  

    #Check sizes were calculated
    assert partition.sizeTotal > 0
    assert partition.sizeAvail > 0
    assert partition.sizeUsed > 0

#Grab total disk info, inluding calculations from partitions
disks = lsrdisk.getDiskStaticInfo()

def test_disk_static_info():
  for key, disk in disks.items():
    #Check the key identifier is the string for the disk path /dev/sda
    assert isinstance(key, str) == True

    #Check key matches disk path
    assert disk.disk == key

    #Check free and total were calculated
    assert disk.free > 0
    assert disk.total > 0 

    #Check disk information was read and stored correctly
    assert disk.make == "ATA"
    assert disk.model == "ST500DM002-1BD14"
    
def test_disk_active_info():
  for key, disk in disks.items():
    #Check disk read/write are still valid values (aka 0 or greater)
    assert isinstance(disk.read, int) == True
    assert isinstance(disk.write, int) == True
    assert disk.read >= 0
    assert disk.write >= 0




#---------------------------------------------------------#
#                     Network Tests                       #
#---------------------------------------------------------#
lsrnet = LsrNet()

interfaces = lsrnet.getNetworkInfo()

def test_net_interfaces():
  for key, interface in interfaces.items():
    #Check if the interface key is a string indentifer
    assert isinstance(key, str) == True
    #Check that each interface data was successfully converted into a Net object
    assert isinstance(interface, Net)

    # Note -- Check specifc networks in cli printout


#---------------------------------------------------------#
#                  Top  Process Tests                     #
#---------------------------------------------------------#
lsrproc = LsrProc()

processes = lsrproc.getProcessInfo()

def test_processes():
  for key, process in processes.items():
    #Check the key is the number order of processes (0=highest)
    assert isinstance(key, int) == True
    #Check that each of the processes was added to the list as a Process object
    assert isinstance(process, Process) == True
    #Check each process was assigned it's pid
    assert process.pid != ""

    # Note -- Check other vars in cli printout



