#!/usr/bin/env python3


import os, sys, subprocess, logging 
from Gatherer import Gatherer
from LsrClasses import Gpu
import json

class LsrGpu(Gatherer):

  def __init__(self):
    super().__init__()
    self.metricType = "gpu"
    self.gpus = []
    self.metricData = self.gatherMetrics()


  #Gather the static metrics once
  def gatherMetrics(self):
    logging.info("Starting GPU gatherer!")
    
    try:
      self.gpus = self.getGpuInfo()
    except Exception as e:
      logging.error(e)  
      self.gpus = None

    return self.getMetricData()

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests 
  def getMetricData(self):
    metricData = {}
    try:
      for i in range(0,len(self.gpus)):
        metricData[i] = self.gpus[i].__dict__
    except:
      metricData = None   
    return json.dumps(metricData)

  #Gather the GPU static information, for each graphics card
  def getGpuInfo(self):
    gpus = []
    lspci = self.getParsedCommandOutput("lspci -vnn | grep VGA -A 12")
    name = ""
    make = ""
    driver = ""

    #Read through each line in the parsed lspci, keying off specific words
    for i in range(0,len(lspci)):

      #Skip over empty lines
      if lspci[i] == []:
        continue

      #Search for the Device line
      if lspci[i][1] == "VGA":
        start=False

        #Loop over the line, looking for identifiers 
        for j in range(0,len(lspci[i])):

            #Look for first list item, skip over the identifier
            if ":" in lspci[i][j][-1]:
              start = True
              continue

            #Once the name is found, start parsing into 1 string
            if start:    
              #Look for prog and end the search
              if "prog" in lspci[i][j]:
                name = name.rstrip()
                break
              #Otherwise, add in the string to full name
              else:
                name += lspci[i][j] + " "

      #Retrieve the manufacturer from the subsystem entry
      elif lspci[i][0] == "Subsystem:":
        make = lspci[i][1]

      #Retrieve the active kernel driver being used (nouveau, nvidia, etc)
      elif lspci[i][0] == "Kernel" and lspci[i][1] == "driver":
        driver = lspci[i][4]

        #Create a dictionary once all vars are found
        gpus.append(Gpu(name, make, driver))

        #Reset var names to collect info on multi-gpus
        name = ""
        make = ""
        driver = ""
        start=False
    
    return gpus


if __name__ == "__main__":
  lsr = LsrGpu()
  lsr.connectToServer(lsr.metricType, lsr.metricData)














