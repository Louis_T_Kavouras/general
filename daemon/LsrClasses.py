
class Id:
  def __init__(self, serial, manufacturer="", name="", model=""):
    self.serialId = serial
    self.manufacturer = manufacturer
    self.name = name
    self.model = model

class Host:
  def __init__(self, hostname="", biosVersion="", biosDate="", osRelease="", osKernel="", osArch="", uptime=""):
     self.hostname = hostname 
     self.biosVersion = biosVersion
     self.biosDate = biosDate
     self.osRelease = osRelease 
     self.osKernel = osKernel
     self.osArch = osArch 
     self.uptime = uptime 

class Processor:
  def __init__(self, processor, vendor="", model="", currentSpeed="", ratedSpeed="", coreId="", arch="", idle=0, total=0,utilization=0):
    self.processor = processor
    self.vendor = vendor
    self.model = model
    self.currentSpeed = currentSpeed
    self.ratedSpeed = ratedSpeed
    self.coreId = coreId
    self.arch = arch
    self.idle = idle
    self.total = total
    self.utilization = utilization

class Process:
  def __init__(self,pid="",user="",cpu="",memory="",runtime="",command=""):
    self.pid = pid
    self.user = user
    self.cpu = cpu
    self.memory = memory
    self.runtime = runtime
    self.command = command

class Memory:
  def __init__(self, total=0,available=0,cached=0,swap=0,slots=""):
    self.total = total
    self.available = available
    self.cached = cached
    self.swap = swap
    self.slots = slots

class MemorySlot:
  def __init__(self, slot="",memType="",size=0,speed=0,make="",model=""):
    self.slot = slot
    self.memType = memType
    self.size = size
    self.speed = speed
    self.make = make
    self.model = model


class Disk:
  def __init__(self,disk, free=0, total=0, make="",model="", partitions={}):
    self.disk = disk
    self.free = free
    self.total = total
    self.make = make
    self.model = model
    self.partitions = partitions
    self.read = 0
    self.write = 0
  
class Partition:
  def __init__(self,name,diskType="",sizeTotal="",sizeUsed="",sizeAvail="",mount=""):
    self.name = name
    self.diskType = diskType
    self.sizeTotal = sizeTotal
    self.sizeUsed = sizeUsed
    self.sizeAvail = sizeAvail
    self.mount = mount

class Net:
  def __init__(self, ip=None, mac=None, recv=0.0, xmit=0.0):
    self.ip = ip
    self.mac = mac
    self.recv = recv
    self.xmit = xmit

class Gpu:
  def __init__(self, name="", make="", driver=""):
    self.name = name
    self.make = make
    self.driver = driver
