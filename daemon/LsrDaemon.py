#!/usr/bin/env python3


import os, sys, re, logging, subprocess
import requests
import threading
from LsrId    import LsrId
from LsrHost  import LsrHost
from LsrCpu   import LsrCpu
from LsrMem   import LsrMem
from LsrGpu   import LsrGpu
from LsrDisk  import LsrDisk
from LsrNet   import LsrNet
from LsrProc  import LsrProc


class LsrDaemon:

  def __init__(self):
    print("Starting the LSR Daemon!\n")
    #self.resetLogs()
    #Gather Serial and Machine information	
    ID = LsrId()
    ID.connectToServer(ID.metricType, ID.metricData)

    #Gather Host Environment static information	
    host = LsrHost()
    host.connectToServer(host.metricType, host.metricData)

    #Gather CPU static information	
    cpu = LsrCpu()
    cpu.connectToServer(cpu.metricType, cpu.metricData)

    #Gather Mem static information	
    mem = LsrMem()
    mem.connectToServer(mem.metricType, mem.metricData)

    #Gather Network static information	
    net = LsrNet()
    net.connectToServer(net.metricType, net.metricData)

    #Gather GPU static information	
    gpu = LsrGpu()
    gpu.connectToServer(gpu.metricType, gpu.metricData)

    #Gather Disk static information	
    disk = LsrDisk()
    disk.connectToServer(disk.metricType, disk.metricData)

    self.runThreads()

  def runThreads(self):
    threads = []
    try:
    
      #Threads with arg=True will run active metric methods
      threads.append(threading.Thread(target=LsrHost, args=(True,)))
      threads.append(threading.Thread(target=LsrCpu, args=(True,)))
      threads.append(threading.Thread(target=LsrMem, args=(True,)))
      threads.append(threading.Thread(target=LsrDisk, args=(True,)))
      threads.append(threading.Thread(target=LsrNet, args=(True,)))
      threads.append(threading.Thread(target=LsrProc, args=(True,)))
      
      for each in threads:
        each.start()
    except:
      print("Unable to start threads")


  def resetLogs(self):
    os.system("mv lsr.log lsr.bkp")
    #os.system("touch lsr.log")


if __name__ == "__main__":
  LsrDaemon()


