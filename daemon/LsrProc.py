#!/usr/bin/env python3


import os, sys, subprocess, logging
from Gatherer import Gatherer
from LsrClasses import Process
import shlex
import time
import json


class LsrProc(Gatherer):

  def __init__(self,active=False):
    super().__init__()
    self.metricType = "proc"
    self.processes = {}
    if active:
      self.gatherActiveMetrics()
    else:
      self.metricData = self.gatherMetrics()



  #Gather the active processes once
  def gatherMetrics(self):
    logging.info("Starting Process gatherer!")
    self.getProcessInfo()
    return self.getMetricData()


  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests
  def getMetricData(self):
    metricData = {}
    for i in range(0, len(self.processes)): 
      metricData[i] = self.processes[i].__dict__
    return json.dumps(metricData)

  #Gather active metrics in a loop
  def gatherActiveMetrics(self):                                                                                     
    while True:                                                                                                      
      self.connectToServer(self.metricType, self.gatherMetrics())
      time.sleep(10)


  #Retrieves the list of top processes
  def getProcessInfo(self):
    processes = []
    try:
      #Retrieves the output from 'top'
      top = self.getParsedCommandOutput("top -b -n 1 | head -27")
      #Processes start on line 7 in top output. This works for 0-20 processes
      for p in range(7,len(top)):
        #--Definitions--
        #p[0] = PID
        #p[1] = User
        #p[2] = Pr
        #p[3] = Ni
        #p[4] = Virt
        #p[5] = Res
        #p[6] = Shr
        #p[7] = S
        #p[8] = %CPU
        #p[9] = %Mem
        #p[10] = Time
        #p[11] = Command

        #Add a process object to the list, in order from top->bottom, using defs above
        processes.append(Process(top[p][0], top[p][1], top[p][8], top[p][9], top[p][10], top[p][11]))

      #Add to the dictionary, 0 being the top process
      for i in range(0, len(processes)):
        self.processes[i] = processes[i]

      return self.processes

    except Exception as e:                                                                                           
      logging.error(e)
      return None


if __name__ in "__main__":
  lsr = LsrProc()
  lsr.connectToServer(lsr.metricType, lsr.metricData)















