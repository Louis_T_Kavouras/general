echo "Starting Shell testing each python script in series"

echo "Clearing lsr.log"
echo "" > lsr.log

echo "Machine ID Test"
python3 LsrId.py
echo "Host Test"
python3 LsrHost.py static
echo "CPU Static"
python3 LsrCpu.py static
echo "CPU Active"
python3 LsrCpu.py active
echo "GPU Static"
python3 LsrGpu.py
echo "Disk Static"
python3 LsrDisk.py static
echo "Disk Active"
python3 LsrDisk.py active
echo "Memory Static"
python3 LsrMem.py static
echo "Memory Active"
python3 LsrMem.py active
echo "Network Static"
python3 LsrNet.py static
echo "Network Active"
python3 LsrNet.py active
echo "Processes"
python3 LsrProc.py
echo ""
