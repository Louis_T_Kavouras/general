#!/usr/bin/env python3


import os, sys, subprocess, logging 
from Gatherer import Gatherer
from LsrClasses import Net
import shlex
import time
import json

class LsrNet(Gatherer):

  def __init__(self,active=False):
    super().__init__()
    self.metricType = "net"
    self.interfaces = {}
    if active:
      self.gatherActiveMetrics()
    else:
      self.metricData = self.gatherMetrics()



  #Gather both the static and active metrics once
  def gatherMetrics(self):
    logging.info("Starting Network gatherer!")
    self.interfaces = self.getNetworkInfo()
    self.getThroughput(2)
    return self.getMetricData()
    
  #Gather active metrics in a loop
  def gatherActiveMetrics(self):
    self.interfaces = self.getNetworkInfo()
    while True:
      self.connectToServer("netActive", self.getActiveMetricData(5))
      time.sleep(10)

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests
  def getMetricData(self):
    metricData = {}
    try:
      for key, inter in self.interfaces.items():
        metricData[key] = inter.__dict__
    except Exception as e:
      logging.error(e)
      metricData=None
    return json.dumps(metricData)


  #Parse the active data into a dicitonary, and covert to JSON before it is sent via requests
  def getActiveMetricData(self,delay=5):
    try:
      metricData = {}
      self.getThroughput(delay)
      for key, interface in self.interfaces.items():
        try:
            metricData[key] = {'recv' : interface.recv, 'xmit': interface.xmit}
        except:
            metricData[key] = {'recv' : None, 'xmit': None}
      return json.dumps(metricData)

    except Exception as e:
      logging.error(e)
      return None

  #Retieve the network info (ip, mac) for each interface on the system
  def getNetworkInfo(self):
    interfaces = {}
    try:
      #Retrives the interface and ipv4 addr
      ipaddr = self.getParsedCommandOutput("ip -4 -o addr")

      #Loop through each line with an ipv4 
      for a in ipaddr:
        try:
          #Add the interface to dictionary with name as key and ipv4 addr value
          interfaces[a[1]] = Net(a[3], None,  0.0,  0.0)
        except Exception as e:
          logging.error(e)

      #Retrives the MAC addrs, splits output into list based on newlines 
      ipmac = self.getParsedCommandOutput("ip -o link")

      #Loop through each line with a link (mac) 
      for m in ipmac:
        try:
          interface = m[1].strip(':')
          #Add mac addr to existing dictionary
          if interface in interfaces:
            interfaces[interface].mac = m[-3]

          #If IP addr wasn't found, add mac and create empty string for IP
          else:
            interfaces[interface] = Net(None, m[-3], 0.0, 0.0)

        except Exception as e:
          logging.error(e)

      return interfaces

    except Exception as e:
      logging.error(e)
      return None


  #Read the recv and transmit on each network interface
  def getThroughput(self, interval):
    try:
        #Get initial reading of interfaces
        net1 = self.getAllLines("/proc/net/dev")
        
        #Defualt to 1 sec interval if invalid arg passed
        try:
          time.sleep(int(interval))
        except:
          interval=1
          time.sleep(interval)

        #Get second reading of interfaces
        net2 = self.getAllLines("/proc/net/dev")

        #Start on range=2 to ignore headers
        for i in range(2,len(net1)):
          data1= shlex.split(net1[i])
          data2= shlex.split(net2[i])

          #Get the interface
          interface1 = data1[0].strip(":")
          interface2 = data2[0].strip(":")

          if interface1 != interface2:
            raise Exception("ERROR comparing /proc/net/dev files")

          #Assign the recv/transmit values from each interface read to floats
          recv1= float(data1[1])
          recv2= float(data2[1])
          xmit1 = float(data1[9])
          xmit2 = float(data2[9])
          
          #Only work on interfaces that are receiving traffic
          if recv2 > 0:
            #Calculate delta since last reading, except executed if first reading and there is no entry yet
            dRecv = (recv2 - recv1) / interval
            dXmit = (xmit2 - xmit1) / interval
  
            #Store, rounding to 2 decimals
            self.interfaces[interface1].recv = round(dRecv/1000,2)
            self.interfaces[interface1].xmit = round(dXmit/1000,2)

        return self.interfaces

    except Exception as e:
        logging.error(e)
        return None


if __name__ in "__main__":
  lsr = LsrNet()
  try:
    if sys.argv[1] == "active":
      lsr.connectToServer("netActive", lsr.getActiveMetricData(3))
    elif sys.argv[1] == "static":
      lsr.connectToServer(lsr.metricType, lsr.metricData)
    else:
      print("No args detected!")
  except:
    print("No args detected!")

