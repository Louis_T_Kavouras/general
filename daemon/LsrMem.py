#!/usr/bin/env python3


import os, sys, subprocess, logging, math, time 
from Gatherer import Gatherer
from LsrClasses import Memory, MemorySlot
import json

class LsrMem(Gatherer):

  def __init__(self,active=False):
    super().__init__()
    self.metricType = "mem"
    self.total = None
    self.available = None
    self.cached = None
    self.swap = None
    self.memorySlots = []
    if active:
      self.gatherActiveMetrics()
    else:
      self.metricData = self.gatherMetrics()

  #Gather both the static and active metrics once
  def gatherMetrics(self):
    logging.info("Starting Memory gatherer!")
    self.getMemInfo()
    self.getMemorySlotInfo()
    self.memory = Memory(self.total, self.available,  self.cached, self.swap, self.memorySlots)
    return self.getMetricData()
  
  #Gather active metrics in a loop
  def gatherActiveMetrics(self):
    while True:  
      self.connectToServer("memActive", self.getMemInfo())
      time.sleep(10)

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests 
  def getMetricData(self):
    metricData = self.memory.__dict__
    for i in range(0, len(metricData['slots'])):
      metricData['slots'][i] = metricData['slots'][i].__dict__
    return json.dumps(metricData)


  #Gather the Memory active information
  def getMemInfo(self):

    #Read /proc/meminfo for total/free/cached memory, throw error if fails to open or invalid order
    try:
      meminfo = self.getAllLines("/proc/meminfo")
      if "MemTotal" not in meminfo[0]:
        raise Exception("MemTotal not found in first line of /proc/meminfo!")
    except Exception as e:
      logging.error(e)
      return json.dumps({"total" : None, "available": None, "cached": None, "swap": None})
     
		#Read through each line in meminfo, keying off specific words
    try:
      for line in meminfo:

        #Check line for total, multiple to convert into Bytes
        if "MemTotal" in line:
          try:
            self.total = math.trunc(int((self.parseInfoLine(line).lstrip(" ")).split(" ")[0]) * 1024 / 1000000)
          except:
            self.total = None

        #Check line for free, multiple to convert into Bytes
        elif "MemFree" in line:
          try:
            self.available = math.trunc(int((self.parseInfoLine(line).lstrip(" ")).split(" ")[0]) * 1024 / 1000000)
          except:
            self.available = None

        #Check line for cached, multiple to convert into Bytes
        elif "Cached" in line:
          try:
            self.cached = math.trunc(int((self.parseInfoLine(line).lstrip(" ")).split(" ")[0]) * 1024 / 1000000)
          except:
            self.cached = None

        #Check line for Swapped total, multiple to convert into Bytes
        elif "SwapTotal" in line:
          try:
            swapTotal = int((self.parseInfoLine(line).lstrip(" ")).split(" ")[0]) * 1024
          except:
            swapTotal = None

        #Check line for amount not swapped, multiple to convert into Bytes, then megabytes
        elif "SwapFree" in line:
          try:
            swapFree = int((self.parseInfoLine(line).lstrip(" ")).split(" ")[0]) * 1024
            self.swap = math.trunc((swapTotal - swapFree) / 1000000)
          except:
            self.swap = None
          #Break out of loop since all metrics were gathered 
          break

      return json.dumps({"total" : self.total, "available": self.available, "cached": self.cached, "swap": self.swap})

    except Exception as e:
      logging.error(e)
      return json.dumps({"total" : None, "available": None, "cached": None, "swap": None})



	#Read through each line in parsed dmidecode (ROOT), keying off specific words to get DIMM info
  def getMemorySlotInfo(self):

    try:

      #Read Memory info (ROOT)
      dm = self.getParsedCommandOutput("sudo dmidecode -t 17")

      #Clear readings from previous run
      self.memorySlots = []

      #Create lists to store data in logical order
      slots = []
      types = []
      sizes = []
      speeds = []
      makes = []
      models = []

      #Read through the dmidecode response, adding each DIMM value, in order, to the list
      for line in dm:

        #ignore the empty lines
        if not line:
          pass

        #Find the DIMM Slot information
        elif line[0] == "Locator:":
          try:
            #Add entries with spaces
            slot = ""
            for i in range(1,len(line)-1):
              slot += line[i] + " "
            slot = slot + line[-1]
            slots.append(slot)
          except Exception as e:
            logging.error(e)
            slots.append(None)
        
        #Find the DIMM Type
        elif line[0] == "Type:" and line[1] != "Detail:":
          try:
            types.append(line[1])
          except:
            types.append(None)

        #Find the DIMM Size
        elif line[0] == "Size:":
          try:
            if line[1] == "No":
              sizes.append(None)
            else:
              size = int(line[1])
              unit = line[2]

              #Database default is in MB , so add cases here to account for different ram sizes
              if unit.lower() == "mb":
                size = size
              elif unit.lower() == "gb":
                size = size * 1000
              elif unit.lower() == "kb":
                size = size / 1000
              elif unit.lower() == "tb":
                size = size * 1000 * 1000
              sizes.append(size)
          except:
            sizes.append(0)

        #Find the DIMM Speed
        elif line[0] == "Speed:":
          try:
            if line[1] == "Unknown":
              speeds.append(0)
            else:
              speed = int(line[1])
              unit = line[2]

              #Database default is in MHz , so add cases here to account for different speeds
              if unit.lower() == "mhz":
                speed = speed
              elif unit.lower() == "ghz":
                speed = speed * 1000
              elif unit.lower() == "khz":
                speed = speed / 1000
              speeds.append(speed)
          except:
            speeds.append(0)

        #Find the DIMM Manufacturer
        elif line[0] == "Manufacturer:":
          try:
            make = ""
            for i in range(1,len(line)-1):
              make += line[i] + " "
            #Add final entry with no trailing space
            make = make + line[-1]
            makes.append(make)
          except:
            makes.append(None)

        #Find the DIMM Model
        elif line[0] == "Part":
          try:
            if line[2] == "Not":
              models.append(None)
            else:
              models.append(line[2])
          except:
            models.append(None)

      #Store each items into a Memory object, to be converted into a dictionary, but in sequence 
      try:
        for i in range(0,len(slots)):
           self.memorySlots.append(MemorySlot(slots[i],types[i],sizes[i],speeds[i],makes[i],models[i])) 
      #Will except if the amount of lines for each slot don't line up
      except Exception as e:
        logging.error(e)
        self.memorySlots = None
          
      return self.memorySlots    

    except Exception as e:
      logging.error(e)
      return None


if __name__ == "__main__":
  lsr = LsrMem()
  try:
    if sys.argv[1] == "active":
      lsr.connectToServer("memActive", lsr.getMemInfo())
    elif sys.argv[1] == "static":                                                                                    
      lsr.connectToServer(lsr.metricType, lsr.metricData)
    else:
      print("No args detected!")
  except:
    print("No args detected!")

