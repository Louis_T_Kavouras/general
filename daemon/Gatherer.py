import subprocess, shlex, sys
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import json
import logging 

class Gatherer:

  def __init__(self):
    self.init_logging()
    #logging.basicConfig(filename='lsr.log2', format='%(levelname)s |%(asctime)s|: %(filename)s in %(funcName)s: \n   %(message)s', level=logging.DEBUG)
    self.getCentralServerIp()
    self.serial = self.getSerial()

    #Abort if unable to read serial ID, as it is the key identifier for system transactions
    if self.serial == None or self.serial == "None" or self.serial == " ":
      print("Unable to read serial.txt file! Run LsrId once as root!")
      logging.error("Unable to read serial.txt file! Run LsrId once as root!")
      sys.exit(1)

  def init_logging(self):
    logging.basicConfig(filename='lsr.log', format='%(levelname)s |%(asctime)s|: %(filename)s in %(funcName)s: \n   %(message)s', level=logging.DEBUG)

  def getSerial(self):
    #---Test Case---
    #return "2UA00317NK"
    try:
      return self.getSingleLine('serial.txt')
    except Exception as e:
      logging.error(e)
      return None

  def getCentralServerIp(self):
    try:
      self.ip = self.getSingleLine('serverIp.txt')
      if self.ip == None:
        self.ip = "127.0.0.1"
    except Exception as e:
      logging.error(e)
      self.ip = "127.0.0.1"

  def connectToServer(self, metricType, metricData):
    try:
      requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
      url = "http://" + self.ip + ":5000/input/" + self.serial + "/" + metricType
      #---Test Case---
      logging.debug("URL:" +  url)
      logging.debug("Pre-sent data:" + str(metricData))
      r = requests.post(url, verify=False, data=metricData, timeout=10)
      if not r.ok:
        logging.error("ERROR: Status code " +  str(r.status_code) + " was returned.")
      else:
        response = r.json()
        logging.info(str(response))

    except Exception as e:
      logging.info(e)


  def getSingleLine(self, inputFile):
    #---Test Case---
    #return None
    try:
      with open(inputFile, 'r') as FILE: 
        line = FILE.readline() 
      FILE.close() 
 
      return line.strip('\n') 
 
    except Exception as e: 
      logging.error(e)
      return None 


  def getAllLines(self, inputFile):
    #---Test Case---
    #return None
    try:
      with open(inputFile, 'r') as FILE: 
        lines = FILE.readlines() 
      FILE.close() 
 
      #Remove newline characters from memory buffer
      for line in lines:
        line = line.strip('\n') 
    
      return lines
 
    except Exception as e: 
      logging.error(e)
      return None 


  def getCommandOutput(self, command):
    #---Test Case---
    #return None
    try:
      data = subprocess.check_output(command, universal_newlines=True, shell=True, timeout=5)
      return data
    except Exception as e:
      logging.error(e)
      return None 
    
  def getOneLineCommandOutput(self, command):
    #---Test Case---
    #return None
    try:
      data = subprocess.check_output(command, universal_newlines=True, shell=True, timeout=5)
      return data.strip("\n")
    except Exception as e:
      logging.error(e)
      return None 

  def getParsedCommandOutput(self, command):
    #---Test Case---
    #return None
    try:
      data = subprocess.check_output(command, universal_newlines=True, shell=True, timeout=5)

      #Strips the last newline to prevent empty entry, then splits output into list based on newlines 
      data = (data.strip("\n")).split("\n")

      #Parse the white space out of the outputs 
      for i in range(0,len(data)):
        data[i] = shlex.split(data[i])

      return data
    except Exception as e:
      logging.error(e)
      return None 

  #Split  entries into key:value, remove newlines, return value
  def parseInfoLine(self,line):
    #---Test Case---
    #return None
    try:
      value = line.split(": ")
      return value[1].strip('\n')
    except:
      return None


