#!/usr/bin/env python3


import os, sys, subprocess, logging 
from Gatherer import Gatherer
from LsrClasses import Host
import json
import time

class LsrHost(Gatherer):

  def __init__(self,active=False):
    super().__init__()
    self.metricType = "host"
    self.host = ""
    if active:
      self.gatherActiveMetrics()
    else:
      self.metricData = self.gatherMetrics()

  #Gather static metrics once
  def gatherMetrics(self):
    logging.info("Starting HostEnv gatherer!")
    self.host = Host(
        self.getHostname(),
        self.getBiosVersion(),
        self.getBiosDate(),
        self.getOsRelease(),
        self.getOsKernel(),
        self.getOsArch(),
        self.getUptime()
      )
    return self.getMetricData()

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests 
  def getMetricData(self):
    return json.dumps(self.host.__dict__)

  def gatherActiveMetrics(self):  
    while True:
      self.connectToServer("uptime", json.dumps({'uptime':self.getUptime()}))
      #Sleep for a day before reporting uptime again  
      time.sleep(86400)
    
  # ----------------------------------------------------------#
  # Metric METHODS for reading metrics from files and and cli #
  # ----------------------------------------------------------#
  def getHostname(self):
    try:
      return self.getCommandOutput("hostname").strip('\n')
    except:
      return None

  def getBiosVersion(self):
    try:
      return self.getSingleLine("/sys/devices/virtual/dmi/id/bios_version")
    except:
      return None

  def getBiosDate(self):
    try:
      return self.getSingleLine("/sys/devices/virtual/dmi/id/bios_date")
    except:
      return None

  def getOsRelease(self):
    try:
      release = self.getCommandOutput("lsb_release -d").strip('\n')
      return release.strip("Description:\t")
    except:
      return None

  def getOsKernel(self):
    try:
      return self.getCommandOutput("uname -r").strip('\n')
    except:
      return None

  def getOsArch(self):
    try:
      return self.getCommandOutput("uname -p").strip('\n')
    except:
      return None

  def getUptime(self):
    try:
      return self.getCommandOutput("uptime -s").strip('\n')
    except:
      return None

  # ----------------------------------------------------------#

if __name__ == "__main__":
  lsr = LsrHost()      
  try:
    if sys.argv[1] == "active":
      lsr.connectToServer("uptime", lsr.gatherActiveMetrics())
    elif sys.argv[1] == "static":
      lsr.connectToServer(lsr.metricType, lsr.metricData)
    else:
      print("No args detected!")
  except:
    print("No args detected!")


