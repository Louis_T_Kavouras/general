#!/usr/bin/env python3


import os, sys, subprocess, logging, shlex 
from LsrClasses import Processor
from Gatherer import Gatherer
import time
import json

class LsrCpu(Gatherer):

  def __init__(self,active=False):
    super().__init__()
    self.metricType = "cpu"
    self.processors = {}
    self.ratedSpeed = self.getRatedSpeed()
    if active:
      self.gatherActiveMetrics()
    else:
      self.metricData = self.gatherMetrics()

  #Gather both the static and active metrics once
  def gatherMetrics(self):
    logging.info("Starting CPU gatherer!")
    self.processors = self.getCpuInfo()
    self.getUtilization()
    time.sleep(2)
    self.getUtilization()
    return self.getMetricData()
    

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests 
  def getMetricData(self):
    metricData = {}
    for key,cpu in self.processors.items():
      metricData[key] = cpu.__dict__
    return json.dumps(metricData)
  
  #Gather active metrics in a loop
  def gatherActiveMetrics(self):
    self.processors = self.getCpuInfo()
    while True:                                                                                                      
      self.getUtilization()
      time.sleep(3)
      self.getUtilization()
      self.connectToServer("cpuActive", self.getMetricData())
      time.sleep(10)


  #Gather the CPU static information, for each processing unit
  def getCpuInfo(self):
    processors = {}
    procinfo = self.getAllLines("/proc/cpuinfo")
    
    #Check if file can be opened, terminate if not since data is not valid
    try:
      if "processor" not in procinfo[0]:
        logging.error("Error finding processor lines in /proc/cpuinfo!")
        sys.exit()
    except Exception as e:
      logging.error(e)
      sys.exit()

    #Read through each line in cpuinfo, keying off specific words
    for i in range(0,len(procinfo)):
      if "processor" in procinfo[i]:
        
        procId = self.parseInfoLine(procinfo[i])

        #Check if the processor is actually a number, otherwise throw error
        try:
          procId = int(procId)
          processor = Processor(procId) 
        except:
          logging.error("Error reading processor number: " + str(procId))
          continue

        #Check next line for Manufacturer
        if "vendor_id" in procinfo[i+1]:
          processor.vendor = self.parseInfoLine(procinfo[i+1])
        
        #Check line for Model
        if "model name" in procinfo[i+4]:
          processor.model = self.parseInfoLine(procinfo[i+4])

        #Check line for Current CPU speed
        if "cpu MHz" in procinfo[i+7]:
          processor.currentSpeed = self.parseInfoLine(procinfo[i+7])
  
        #Check line for Core ID
        if "core id" in procinfo[i+11]:
          processor.coreId = self.parseInfoLine(procinfo[i+11])
        
        #Check line for Intel/AMD 32 or 64-bit arch
        if "flags" in procinfo[i+19]:
          flags = self.parseInfoLine(procinfo[i+19])
          if " lm " in flags:
            processor.arch = "x64"
          else:
            processor.arch = "x86" 

        processor.ratedSpeed = self.ratedSpeed
        processors[procId] = processor

    return processors

  #Read dmesg for rated speed of the processor 
  def getRatedSpeed(self):
    try:
        dmesg = self.getAllLines("/var/log/dmesg")
        for line in dmesg:
          if "MHz processor" in line:
            ratedSpeed = line.split(' ')[-3]
            break
        return ratedSpeed
    except Exception as e:
        logging.error(e)
        return None


  #Retrieve the percentage usage of each logical processing unit since the last reading and store 
  def getUtilization(self):

    try:
        #Read all of /proc/stat and loop through for each CPU
        stat = self.getAllLines("/proc/stat")
        for i in range(1,len(stat)):
          #leave loop after reading cpu values
          if "cpu" not in stat[i]:
            break
          else:
            data= shlex.split(stat[i])
            #Get the cpu #
            cpu = int(data[0].strip("cpu"))
            #Retrieve the idle time
            idle = float(data[4])
            #Add every column to calculate the total
            total = 0
            for value in range(1, len(data)):
              total += float(data[value])

            #Calculate difference between last read, and store current values into cpu structure
            dIdle = idle - self.processors[cpu].idle              
            self.processors[cpu].idle = idle               
            dTotal = total - self.processors[cpu].total 
            self.processors[cpu].total = total
            
            #Avoid divide by 0 if theres no difference
            if dIdle == dTotal:
              (self.processors[cpu]).utilization = 0

            #Store new utilization into the correct proccesor
            else:
              (self.processors[cpu]).utilization = round(100 * (1 - (dIdle / dTotal)),2)

        return self.processors

    except Exception as e:
        logging.error(e)
        return None


if __name__ == "__main__":
  lsr = LsrCpu()

  try:
    if sys.argv[1] == "active":
      lsr.connectToServer("cpuActive", lsr.getMetricData())
    elif sys.argv[1] == "static":
      lsr.connectToServer(lsr.metricType, lsr.metricData)
    else:
      print("No args detected!")
  except:
    print("No args detected!")



















