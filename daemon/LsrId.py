#!/usr/bin/env python3

import os, sys, subprocess, logging 
from Gatherer import Gatherer
from LsrClasses import Id
import json

class LsrId(Gatherer):

  def __init__(self):
    self.init_logging()
    self.metricType = "id"
    self.metricData = self.gatherMetrics()

    #Since ID needs to run before getSerial can run in Gatherer, skip running the 
    # inherited __init__ function until after
    super().__init__()


  #Gather both the static metrics once
  def gatherMetrics(self):
    logging.info("Starting ID gatherer!")

    self.Id = Id( 
      self.getSerial(),
      self.getManufacturer(),
      self.getName(),
      self.getModel()
    )
    return self.getMetricData()

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests
  def getMetricData(self):
    return json.dumps(self.Id.__dict__)

  #Use sudo to run as root user and read out the contents of the product serial with cat
  def getSerial(self):
      serial = self.getOneLineCommandOutput("sudo cat /sys/devices/virtual/dmi/id/product_serial")
      serial = serial.strip()
      if serial:
        self.getCommandOutput("echo " + serial + " > serial.txt")
      return serial

  #Read manufacturer information from the file
  def getManufacturer(self):
      return self.getSingleLine("/sys/devices/virtual/dmi/id/sys_vendor")

  #Read system name from the file
  def getName(self):
      return self.getSingleLine("/sys/devices/virtual/dmi/id/product_name")

  #Read system model from the file
  def getModel(self):
      return self.getSingleLine("/sys/devices/virtual/dmi/id/product_version")


if __name__ == "__main__":
  lsr = LsrId()
  lsr.connectToServer(lsr.metricType, lsr.metricData)





