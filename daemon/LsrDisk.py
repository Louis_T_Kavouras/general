#!/usr/bin/env python3


import os, sys, subprocess, logging 
from Gatherer import Gatherer
from LsrClasses import Disk
from LsrClasses import Partition
import shlex
import time
import json

class LsrDisk(Gatherer):

  def __init__(self,active=False):
    super().__init__()
    self.metricType = "disk"
    self.disks = {}
    self.partitions = {}
    self.filesystems = ["ext3", "ext4"]
    if active:
      self.gatherActiveMetrics()
    else:
      self.metricData = self.gatherMetrics()


  #Gather both the static and active metrics once
  def gatherMetrics(self):
    logging.info("Starting Disk gatherer!")
    self.getDiskStaticInfo()
    self.getThroughput(2)
    return self.getMetricData()
        
  #Gather active metrics in a loop
  def gatherActiveMetrics(self):
    self.getDiskStaticInfo()
    while True:                                                                                                      
      self.connectToServer("diskActive", self.getActiveMetricData(5))
      time.sleep(10)

  #Parse the data into a dicitonary, and covert to JSON before it is sent via requests
  def getMetricData(self):
    metricData = {}
    for key, disk in self.disks.items():
      metricData[key] = disk.__dict__
      for part,data in metricData[key]['partitions'].items():
       metricData[key]['partitions'][part] =  data.__dict__
      metricData[key] = metricData[key]
    return json.dumps(metricData)


  #Parse the active data into a dicitonary, and covert to JSON before it is sent via requests
  def getActiveMetricData(self,delay=5):
    try:
      metricData = {}
      self.getThroughput(delay)
      for key, disk in self.disks.items():
        metricData[key] = {'disk' : disk.disk, 'read': disk.read, 'write': disk.write}
      return json.dumps(metricData)
    except Exception as e:
      logging.error(e)
      return None

  #Gather the Disk Static information
  def getDiskStaticInfo(self):
    try:
      lsscsi = self.getParsedCommandOutput("lsscsi")
      #Iterate through the outputs and only add items labeled "disk" into the dictionary
      for disk in lsscsi:
        if disk[1] == "disk":
          total, partitions = self.getPartitions(disk[5])
          #Calculate total free space from availability in the partitions
          free = 0
          for key,partition in partitions.items():
              free += partition.sizeAvail
          
          self.disks[disk[5]] = Disk(disk[5], free, total, disk[2], disk[3], partitions)

      return self.disks
    except Exception as e:
      logging.error(e)
      return None

  #Gather the partiton info for each disk in the system
  def getPartitions(self,disk):
    partitions = {}
    total = None
    try:
      lsblk = self.getParsedCommandOutput("lsblk -o type,name,fstype,mountpoint,size -n -p -r -b " + disk)

      #Gets the total size of the size, in megabytes
      total = round(int(lsblk[0][2]) / 1000000)

      #Start at range=1, 0 is the original disk (/dev/sda)
      for i in range(1,len(lsblk)):
        #Check if it's a partition or something else
        if lsblk[i][0] == "part":
          name=lsblk[i][1] 
          diskType=lsblk[i][2] 

          #Checks if the line contains a mount point or is empty
          if len(lsblk[i]) > 4:
            sizeTotal= lsblk[i][4] 
            mount=lsblk[i][3] 
            sizeUsed = round(self.getUsed(mount) / 1000000)
            sizeAvail = round(self.getAvailable(mount) / 1000000)
          else:
            sizeTotal= lsblk[i][3] 
            mount=None 
            sizeUsed = None
            sizeAvail = None

          #Convert total size into a number from string
          try:
            sizeTotal = round(int(sizeTotal) / 1000000)
          except Exception as e:
            logging.error(e)
            sizeTotal = None

          #Check if a regular partition
          if "ext" in diskType:
            partitions[name] = Partition(name,diskType,sizeTotal,sizeUsed, sizeAvail,mount) 

          #If its a logical volume, sum up the available and used sizes in each mounted location
          elif "LVM" in diskType:
            sizeUsed, sizeAvail = self.getLvmSizes(lsblk,i)
            partitions[name] = Partition(name,diskType,sizeTotal,sizeUsed,sizeAvail,mount) 

      return total, partitions

    except Exception as e:
      logging.error(e)
      return None, None


  #Calculates the size availale within the LVM
  def getLvmSizes(self,lsblk,i):
    sizeAvail = 0
    sizeUsed = 0
    try:
      #Look for disks with the lvm tag, starting at i (current disk from lsblk)
      for j in range(i+1,len(lsblk)):

        #If no longer in the lvm, stop search
        if lsblk[j][0] != "lvm":
          break
        #Otherwise, loop for each lvm member and add the sizes used if a 'ext' disk (ignore swap)
        else:
          if "ext" in lsblk[j][2]: 
            used = self.getUsed(lsblk[j][1])
            if used != None:
              sizeUsed = round((sizeUsed + used)/ 1000000)

            avail = self.getAvailable(lsblk[j][1])
            if avail != None:
              sizeAvail = round((sizeAvail + avail) / 1000000)
      return sizeUsed, sizeAvail
      
    except Exception as e:
      logging.error(e)
      return None, None

  
  #Retrieves the free size for a mounted disk
  def getAvailable(self, mount):
    try:
      df = self.getCommandOutput("df -B1 --output=avail " + str(mount) + " | grep -v Avail")
      return int(df.strip("\n"))
    except Exception as e:
      logging.error(e)
      return None
      
  #Retrieves the used size for a mounted disk
  def getUsed(self, mount):
    try:
      df = self.getCommandOutput("df -B1 --output=used " + str(mount) + " | grep -v Used")
      return int(df.strip("\n"))
    except Exception as e:
      logging.error(e)
      return None
    

  #filesystems located on the system
  def getFilesystems(self):
    filesystems = {}
    
    try:
      #Baseline df command
      command = "df -B1 -T"
      
      #Append the supported filesystems to the command
      for sf in self.filesystems:
        command += " --type=" + sf 

      df = self.getParsedCommandOutput(command)
      
      #Remove the header line
      df.pop(0)
    
      #For each mounted filesystem retrieved by 'df'
      for fs in df:

        #Remove leading directories from the path (/dev/sda1 -> sda1)
        name = (fs[0].split('/'))[-1]

        #Parse into dictionary based on location in line
        filesystem = {
          'name' : name,
          'used' : fs[3],
          'avail' : fs[4],
          'mount' : fs[6]
        }

        filesystems[name] = filesystem 
      return filesystems

    except Exception as e:
      logging.error(e)
      return None

  #Read the active read / write data for the disks
  def getThroughput(self, interval):
    try:
        #Get initial reading of disk
        diskstats1 = self.getAllLines("/proc/diskstats")
        
        #Defualt to 1 sec interval if invalid arg passed
        try:
          time.sleep(int(interval))
        except:
          interval=1
          time.sleep(interval)

        #Get second reading of interfaces
        diskstats2 = self.getAllLines("/proc/diskstats")
  
        #Get valid disks to use
        disks = list(self.disks.keys())

        #Grab data for each disk
        for i in (0,len(disks)):
        #Parse out just the shorthand name with no path (sda)
          shortdisk = disks[i].split('/')[-1]

          for i in range(0,len(diskstats1)):
            data1= shlex.split(diskstats1[i])
            data2= shlex.split(diskstats2[i])
      
            #Skip over disks if they are not the current disk being searched for
            if data1[2] != shortdisk:
              continue          
          
            else:
              #Assign the read/write values from each diskstat read to floats
              read1= float(data1[5]) 
              read2= float(data2[5]) 
              write1 = float(data1[9]) 
              write2 = float(data2[9]) 
            
              #Calculate delta between readings, 1 sector = 512 bytes so multiply to get B/s
              dRead = (read2 - read1) / interval * 512
              dWrite = (write2 - write1) / interval * 512

              #Store, rounding to 2 decimals, in kB
              self.disks[disks[i]].read = round(dRead/1000,2)
              self.disks[disks[i]].write = round(dWrite/1000,2)

          return self.disks

    except Exception as e:
        logging.error(e)
        return None



if __name__ in "__main__":
  lsr = LsrDisk()
  try:
    if sys.argv[1] == "active":
      lsr.connectToServer("diskActive", lsr.getActiveMetricData(3))
    elif sys.argv[1] == "static":
      lsr.connectToServer(lsr.metricType, lsr.metricData)
    else:
      print("No args detected!")
  except:
    print("No args detected!")
                                 
















