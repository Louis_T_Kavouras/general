#!/usr/bin/env python3

'''
This project was made for Marist MSCS 710 Final Project to fulfill 
the class requirement of creating a metric collector system. 
'''

__author__ = "Jason Boniello, Lou Kavouras, Alex Fuoco"
__credits__ = ["Jason Boniello", "Lou Kavouras", "Alex Fuoco"]
__license__ = "MIT"
__version__ = "1.0"
__email__ = "Jason.Boniello1@marist.edu"
__status__ = "Production"

import os
import hashlib

from database import Database 
from database import machineId 
from database import hostEnv 
from database import gpuStatic 
from database import processes 
from database import partition 
from database import uptime 
from database import cpuStatic  
from database import cpuActive  
from database import memStatic  
from database import memActive  
from database import diskStatic  
from database import diskActive  
from database import netStatic  
from database import netActive  
from database import User
from database import userAccess

from flask import Flask, render_template, request, redirect, url_for, \
    session, jsonify
import re
import json
from datetime import datetime , timedelta


#Initialize the webserver IP address, reading from WebLink.py
try:
    with open('WebLink.py', 'r') as f:
        webserver = f.readline().strip('\n')
    f.close()
except Exception as e:
    print(e)
    webserver = "127.0.0.1"


# Initialize the Flask application
app = Flask(__name__)
app.secret_key = os.urandom(24)


#-------------------------------------#
#              API Routes             #
#-------------------------------------#

@app.route('/')
@app.route('/index')
@app.route('/index.html')
def index():
    return render_template('index.html')


@app.route('/team')
@app.route('/team.html')
def team():
    return render_template('team.html')


@app.route('/support')
@app.route('/support.html')
def support():
    return render_template('support.html')


@app.route('/auth', methods=["POST", "GET"])
def auth():
    msg = ""
    if request.method == 'POST' and 'email' in request.form and 'password' in \
            request.form:

        #Retrieve email and password from the POST form
        email = request.form['email']
        password = request.form['password']

        #Create user object to fetch information from database
        user = User.User()
        data = user.select_user_by_email(email=email)

        #If the user actually exists in the database (aka was registered)
        if data:
            #Create a password hash from the POST input
            passwordHash = hashlib.pbkdf2_hmac(
                'sha256',
                password.encode('utf-8'),  # Convert the password to bytes
                data['Salt'],
                100000
            )
            #Compare the stored Hash password with the attempted input
            if data['PasswordHash'] == passwordHash:
                #If successful match, establish the session tokens
                session["loggedin"] = True
                session["userid"] = data['UserId']
                session["email"] = data['Email']
                session["username"] = data['UserName']
                session["lname"] = data['LastName']
                session["fname"] = data['FirstName']
                session["isAdmin"] = data['isAdmin']
                session["phone"] = data['Phone']
                return redirect("dashboard.html")

            else:
                msg = 'Incorrect username OR password'
        else:
            msg = 'Incorrect username OR password'

    return render_template('login.html', msg=msg)


@app.route('/dashboard')
@app.route('/dashboard.html')
def dashboard():

    #Check if user is signed in
    try:
        if not session['userid']:
            return redirect('auth')
    except:
        #Except catches if session object not already created
        return redirect('auth')

    machines = []
    try:
        #Grabs a list of all machines user has access to
        accessList = userAccess.select_user_access(session['userid'])
        machineList = machineId.select_all_machines()

        #Print all machines in the access list for the given user
        if accessList:
          for row in accessList:
              for machine in machineList:
                  if row['Serial'] == machine['Serial']:
                      machines.append(machine)
        return render_template('service/dashboard.html', machines=machines)

    except Exception as e:
        print(e)
        return render_template('service/dashboard.html', machines=machines)


@app.route('/users')
@app.route('/users.html')
def users():
    try:
        #Create user object to check if the session has admin privledges to look at the database
        user = User.User()
        isAdmin = user.select_user_by_id(session['userid'])['isAdmin']
        if isAdmin < 1:
            return "You do not have admin access to this page, please return to homepage"
        users = user.select_all_users()
        return render_template("service/users.html", users=users)

    except Exception as e:
        print(e)
        return redirect('auth')


@app.route('/login', methods=["GET", "POST"])
@app.route('/login.html', methods=["GET", "POST"])
def login():
    return render_template("login.html")


@app.route('/register', methods=["GET", "POST"])
@app.route('/register.html', methods=["GET", "POST"])
def register():
    try:
        #Retrieve the FORM submissions from the POST requests
        if request.method == "POST":
            email = request.form["email"]
            password = request.form["password"]
            checkPassword = request.form["checkpassword"]
            username = request.form["username"]
            firstName = request.form["firstname"]
            lastName = request.form["lastname"]
            phone = request.form["phone"]

            #Establish a user object for database interaction
            user = User.User()

            #Generate the Salt and create the password hash
            salt = os.urandom(32)
            passwordHash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)

            #Check that the confirmation password matches the initial password
            if password == checkPassword:
                #Check if there is already a user with the registered email address
                if user.select_user_by_email(email=email) is not None:
                    return render_template('register.html', msg="Email already has an account.")
                else:
                    #If the email is free, insert the user and redirect to signin page
                    user.insert_user(email, passwordHash, salt, username, False, lastName, firstName, phone)
                    return redirect('auth')
            else:
                return render_template('register.html', msg="Passwords don't match.")

        #Return the register page on a GET request
        return render_template('register.html')

    except Exception as e:
        print(e)
        return render_template('register.html', msg=e)


@app.route('/logout')
def logout():
    #Remove all session tokens on logout
    session["loggedin"] = False
    session["userid"] = None
    session["email"] = None
    session["username"] = None
    session["lname"] = None
    session["fname"] = None
    session["fname"] = None
    session["isAdmin"] = None
    session["phone"] = None
    return redirect(url_for('login'))


@app.route('/machine/<serial>')
def machine(serial):

    #Check if user has credentials to access the listed machine
    if not check_user_access(serial):
      return jsonify("ACCESS IS DENIED FOR THIS MACHINE. PLEASE CONTACT ADMIN TO BE ADDED!")

    #Find the database entry id for the machine serial number
    machine = machineId.select_machine(serial)

    #Define what metric cards to create
    metricTypes = {'host': "Host", 'disk' : "Storage", 'cpu': "CPU", 'memory': "Memory", 'gpu': "GPU", 'network': "Network", 'process': "Top Processes"} 

    return render_template("service/machine.html", machine=machine, metricTypes=metricTypes)


@app.route('/machine/<serial>/<metric>')
def machineMetric(serial,metric):

    #Check if user has credentials to access the listed machine
    if not check_user_access(serial):
        return jsonify("ACCESS IS DENIED FOR THIS MACHINE. PLEASE CONTACT ADMIN TO BE ADDED!")

    #Find the database entry id for the machine serial number
    machine = machineId.select_machine(serial)

    #Path for basic system information
    if metric == "host":
        host = hostEnv.select_host_by_serial(serial)
        try:
            up = uptime.select_uptime_by_serial(serial)
            up = up['UptimeValue'].strftime("%m/%d/%Y %H:%M:%S")
        except:
            up = None
        return render_template("service/host.html", host=host, up=up)

    #Path for each processor unit information
    elif metric == "cpu":
        cpus = cpuStatic.select_all_cpus_by_serial(serial)
        for i in range(0,len(cpus)):
            active = cpuActive.select_recent_cpu_active(cpus[i]['CPUStaticID']) 
            try:
                cpus[i]['Utilization'] = active['Utilization']
                cpus[i]['SpeedMHz'] = active['SpeedMHz']
            except:
                cpus[i]['Utilization'] = None
                cpus[i]['SpeedMHz'] = None
        return render_template("service/cpu.html", cpus=cpus, webserver=webserver, serial=serial, metric=metric)

    #Path for total disk stats and individual partition information
    elif metric == "disk":
        disks = diskStatic.select_disk_by_serial(serial)
        for i in range(0,len(disks)):
            try:
                disks[i]['partitions'] = partition.select_partition_by_disk(disks[i]['DiskStaticID']) 
            except:
                disks[i]['partitions'] = None 

            try:
                active = diskActive.select_recent_disk_active(disks[i]['DiskStaticID']) 
                disks[i]['Read'] = active['Read'] 
                disks[i]['Write'] = active['Write']
            except:
                disks[i]['Read'] = None 
                disks[i]['Write'] = None 
        return render_template("service/disk.html", disks=disks, webserver=webserver, serial=serial, metric=metric)

    #Path for memory DIMM information and active readings
    elif metric == "memory":
        static = memStatic.select_mem_static(serial)
        active = memActive.select_recent_mem_active(serial)
        return render_template("service/memory.html", active=active, static=static, webserver=webserver,  serial=serial, metric=metric)

    #Path for GPU information
    elif metric == "gpu":
        gpus = gpuStatic.select_gpu_by_serial(serial)
        return render_template("service/gpu.html", gpus=gpus)

    #Path for network interfaces and usage
    elif metric == "network":
        interfaces = netStatic.select_net_static(serial)
        for i in range(0,len(interfaces)):
            try:
                active =netActive.select_recent_net_active(interfaces[i]['NetStaticID']) 
                interfaces[i]['BytesIn'] = active['BytesIn'] 
                interfaces[i]['BytesOut'] = active['BytesOut']
            except:
                interfaces[i]['BytesIn'] = None 
                interfaces[i]['BytesOut'] = None 

        #Gather TOTAL active data    
        for i in range(0,len(interfaces)):
            try:
                totalActive =netActive.select_all_net_active(interfaces[i]['NetStaticID']) 
                interfaces[i]['TotalIn'] = totalActive['BytesIn'] 
                interfaces[i]['TotalOut'] = totalActive['BytesOut']
            except:
                interfaces[i]['TotalIn'] = None
                interfaces[i]['TotalOut'] = None
        return render_template("service/network.html", interfaces=interfaces, webserver=webserver, serial=serial, metric=metric)

    #Path for last 20 processes from 'top'
    elif metric == "process":
        activeProcesses = processes.select_recent_processes(serial)
        return render_template("service/processes.html", activeProcesses=activeProcesses)

    else:
        return render_template('functional/404.html'), 404


@app.route('/get/<serial>/<metric>', methods=['GET', 'POST'])
def get_serial_metric(serial, metric):
  try:

    #Check if user has credentials to access the listed machine
    if not check_user_access(serial):
      return jsonify("ACCESS IS DENIED FOR THIS MACHINE. PLEASE CONTACT ADMIN TO BE ADDED!")
    
    #Initialize var return value
    stats = ""

    #Retrieve the interface to select data for
    try:
      selectMetric  = request.args.get('metric')
    except Exception as e: 
      print(e)
      return None

    #Retrieve the start and stop timeframe for data, otherwise use the last 15min default
    try:
      startdate = request.args.get('startdate').split(" GMT",1)[0]
      enddate = request.args.get('enddate').split(" GMT",1)[0]
      startdate = datetime.strptime(startdate, "%a %b %d %Y %H:%M:%S")
      enddate = datetime.strptime(enddate, "%a %b %d %Y %H:%M:%S")

    except Exception as e:
      print(e)
      startdate = datetime.now() - timedelta(minutes=90) 
      enddate = datetime.now()
  
    #CPU Metrics
    if metric == "cpu":
      CPUStaticID = cpuStatic.check_serial_processor(serial, selectMetric)['CPUStaticID']
      stats = cpuActive.select_by_day(CPUStaticID,startdate,enddate)

    #Memory Metrics
    elif metric == "memory": 
      stats = memActive.select_by_day(serial,startdate,enddate)

    #Network Metrics
    elif metric == "network": 
      NetStaticID = netStatic.check_serial_interface(serial, selectMetric)['NetStaticID']
      stats = netActive.select_by_day(NetStaticID,startdate,enddate)

    #Disk Metrics
    elif metric == "disk": 
      DiskStaticID = diskStatic.check_serial_disk(serial, selectMetric)['DiskStaticID']
      stats = diskActive.select_by_day(DiskStaticID,startdate,enddate)

    return jsonify(stats)

  except Exception as e:
    print(e)
    return jsonify("ERROR: " + str(e))
    

@app.route('/input/<serial>/<metric>', methods=['GET', 'POST'])
def input_serial_metric(serial, metric):
  try:
    #Accept only the POST Input request
    if request.method == "POST":

      #Load in the data field from the daemon
      data = json.loads((request.data).decode())
                                                                                                                     
      #ID 
      if metric == "id":
        machineId.insert_machine_id(data['serialId'], data['manufacturer'], data['model'], data['name'])

      #Host Environment 
      elif metric == "host":
        hostEnv.insert_host_env(serial, data['hostname'], data['biosVersion'], datetime.strptime(data['biosDate'], '%m/%d/%Y'), data['osRelease'], data['osKernel'], data['osArch'])

      #uptime 
      elif metric == "uptime":
        uptime.insert_uptime(serial, data['uptime'])

      #CPUStatic
      elif metric == "cpu":
          for key,value in data.items():
            cpuStatic.insert_cpu_static(serial, value['processor'], value['coreId'], value['arch'], value['ratedSpeed'], value['vendor'], value['model'])

      #CPUActive
      elif metric == "cpuActive":
          #Grab all staticinfo for the given serial number
          cpus = cpuStatic.select_all_cpus_by_serial(serial)

          #Loop through th return list, and assign the cpustatic id for active table to use
          for cpu in cpus:
            try: 
              data[str(cpu['ProcessorID'])]['CPUStaticID'] = cpu['CPUStaticID']
            except Exception as e:
              print('Serial ' + str(serial) + ", Processor " + str(e) + " doesn't exist!")
               
          for key,value in data.items():
            try:
              cpuActive.insert_cpu_active(value['CPUStaticID'], value['utilization'], value['currentSpeed'])
            except Exception as e:
              print("No static id found for " + serial + ", Processor: " + str(value['processor']))
        
      #MemoryStatic
      elif metric == "mem":
        for value in data['slots']:
          memStatic.insert_mem_static(serial, value['slot'], value['memType'], value['size'], value['speed'], value['make'], value['model'])
          
      #MemoryActive
      elif metric == "memActive":
        memActive.insert_mem_active(serial, data['total'], data['available'], data['cached'], data['swap'])

      #GPU
      elif metric == "gpu":
        for key,value in data.items():
          gpuStatic.insert_gpu_static(serial, int(key), value['name'], value['make'], value['driver'])

      #DiskStatic
      elif metric == "disk":
        for key,value in data.items():
          try:
            diskStatic.insert_disk_static(serial, value['disk'], value['make'], value['model'], value['free'], value['total'])

            #Add the partitions for the disk, by using the ID from the static table
            diskIds = diskStatic.select_disk_by_serial(serial)
            diskStaticId = None
          
            #Loops through and assigns the cpustaticid for each disk
            for disk in diskIds:
              if disk['Disk'] == value['disk']:
                diskStaticId = disk['DiskStaticID']
                break

            #IF a disk was found, add it's partitions to the table
            if diskStaticId != None:
              for each, part in value['partitions'].items():
                partition.insert_partition(diskStaticId, part['name'], part['mount'], part['sizeAvail'], part['sizeTotal'], part['sizeUsed'], part['diskType'])

          except Exception as e:
            print("No static id found for " + serial + ", Processor: " + str(value['processor']))
            
      #DiskActive
      elif metric == "diskActive":
        #Add the partitions for the disk, by using the ID from the static table
        disks = diskStatic.select_disk_by_serial(serial)
        diskStaticId = None
      
        #Loops through and assigns the cpustaticid for each disk
        for disk in disks:
          if disk['Disk'] in data:
            data[disk['Disk']]['DiskStaticID'] = disk['DiskStaticID']

        for key,value in data.items():
          try:
            diskActive.insert_disk_active(value['DiskStaticID'], value['read'], value['write'])
          except Exception as e:
            print("No static id found for " + serial + ", Disk: " + str(value['disk']))

      #NetworkStatic
      elif metric == "net":
        for key,value in data.items():
          try:
            netStatic.insert_net_static(serial, key, value['ip'], value['mac'])
          except Exception as e:
            print("No static id found for " + serial + ", Interface: " + str(key))

      #NetworkActive
      elif metric == "netActive":
        #Grab all staticinfo for the given serial number
        nets = netStatic.select_net_static(serial)

        #Loop through the return list, and assign the cpustatic id for active table to use
        for net in nets:
          try: 
            data[str(net['Interface'])]['NetStaticID'] = net['NetStaticID']
          except Exception as e:
            print('Serial ' + str(serial) + ", Interface " + str(e) + " doesn't exist!")

        for key,value in data.items():
          try:                                                                         
            netActive.insert_net_active(value['NetStaticID'],value['recv'], value['xmit'])
          except Exception as e:
            print("No static id found for " + serial + ", Interface: " + str(key))

      #Processes
      elif metric == "proc":
        for key,value in data.items():
          try:
            processes.insert_processes(serial, value['pid'], value['user'], value['cpu'], value['memory'], value['runtime'], value['command'])
          except Exception as e:
            print("No static id found for " + serial + ", pid: " + str(value['pid']))

      return jsonify("Server has recieved: " + str(data))

    else:
      return jsonify("GET request was denied")
    
  except Exception as e:
    print(e)
    return jsonify("ERROR: " + str(e))


@app.errorhandler(404)
def page_not_found(e):
    return render_template('functional/404.html'), 404


@app.errorhandler(500)
@app.errorhandler(501)
@app.errorhandler(502)
@app.errorhandler(503)
@app.errorhandler(504)
def page_not_found(e):
    return "<h1> We apologize for the inconvenience, it appears there is " \
           "a server error on our side. (500)</h1>"


def check_user_access(serial):
    try:
        #Check if user has permissions to view the serial
        accessList = userAccess.select_user_access(session['userid'])
        for row in accessList:
            if row['Serial'] == serial:
                return True
    except Exception as e:
        print(e)
        return False 
    return False


if __name__ == '__main__':
    app.run(webserver)
