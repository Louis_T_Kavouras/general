from database import userAccess, machineId, memStatic, memActive, cpuStatic, \
    cpuActive, gpuStatic, diskStatic, diskActive, partition, processes, netStatic, \
    netActive, User, hostEnv
import time

# User = User.User()
#
# # Inserting users
# User.insert_user(email="alex.fuoco1@marist.edu", username="alexfuoco", first_name="Alex", last_name="Fuoco",
#                  is_admin=True, password_hash="fdsanfduianjker")
# User.insert_user(email="Louis.Kavouras1@marist.edu", username="loukavouras", first_name="Lou", last_name="Kavouras",
#                  is_admin=True, password_hash="45254njkluhf948")
# User.insert_user(email="jason.boniello1@marist.edu", username="jasonboniello", first_name="Jason", last_name="Boniello",
#                  is_admin=False, password_hash="nmkolijmnbhulkmi")
#
# # Selecting Users
# print(User.select_user_by_id(1))
# print(User.select_user_by_id(2))
# print(User.select_user_by_id(3))
# print(User.select_user_by_email("alex.fuoco1@marist.edu"))
# print(User.select_user_by_email("Louis.Kavouras1@marist.edu"))
# print(User.select_user_by_email("jason.boniello1@marist.edu"))
#
# # Inserting Machines
# machineId.insert_machine_id(serial="NXGTSAA8820460E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-1")
# machineId.insert_machine_id(serial="OXGTSAA7744859E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-2")
# machineId.insert_machine_id(serial="PXGTSAA0892634E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-3")
# machineId.insert_machine_id(serial="QXGTSAA0912872E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-4")
#
# # Selecting Machines
# print(machineId.select_machine("NXGTSAA8820460E72E7600"))
# print(machineId.select_machine("OXGTSAA7744859E72E7600"))
# print(machineId.select_machine("PXGTSAA0892634E72E7600"))
# print(machineId.select_machine("QXGTSAA0912872E72E7600"))
# print()
#
# # Inserting UserAccess
# # userAccess.insert_user_access(user_id=1, serial="NXGTSAA8820460E72E7600")
# # userAccess.insert_user_access(user_id=1, serial="PXGTSAA0892634E72E7600")
# # userAccess.insert_user_access(user_id=1, serial="QXGTSAA0912872E72E7600")
# # userAccess.insert_user_access(user_id=2, serial="NXGTSAA8820460E72E7600")
# # userAccess.insert_user_access(user_id=2, serial="OXGTSAA7744859E72E7600")
# # userAccess.insert_user_access(user_id=2, serial="PXGTSAA0892634E72E7600")
# # userAccess.insert_user_access(user_id=2, serial="QXGTSAA0912872E72E7600")
# # userAccess.insert_user_access(user_id=3, serial="PXGTSAA0892634E72E7600")
# # userAccess.insert_user_access(user_id=3, serial="QXGTSAA0912872E72E7600")
# #
# # print(userAccess.select_user_access(1))
# # print(userAccess.select_user_access(2))
# # print(userAccess.select_user_access(3))
# # print()
#
# # Inserting Memory Static
# memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=8, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM2', mem_type="SODIMM", unit_size=8, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="OXGTSAA7744859E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=16, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="OXGTSAA7744859E72E7600", dimm_slot='CPU0 DIMM2', mem_type="SODIMM", unit_size=8, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="PXGTSAA0892634E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=8, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="QXGTSAA0912872E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=16, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=8, speed=1600,
#                             make=None, model=None)
# memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=8, speed=1600,
#                             make="UPDATED ROW", model=None)
#
#
# # Reading From Memory Static
# print(memStatic.select_mem_static("NXGTSAA8820460E72E7600"))
# print(memStatic.select_mem_static("OXGTSAA7744859E72E7600"))
# print(memStatic.select_mem_static("PXGTSAA0892634E72E7600"))
# print(memStatic.select_mem_static("QXGTSAA0912872E72E7600"))
# print()
#
# # Memory Active
# memActive.insert_mem_active(serial="NXGTSAA8820460E72E7600", total_size=8000, free=1300, cache=1200, swap=None)
# print(memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600"))
# memActive.insert_mem_active(serial="NXGTSAA8820460E72E7600", total_size=8000, free=1100, cache=1400, swap=None)
# print(memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600"))
# memActive.insert_mem_active(serial="NXGTSAA8820460E72E7600", total_size=8000, free=1800, cache=1100, swap=None)
# print(memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600"))
# print(memActive.select_recent_mem_active(serial="NXGUITJAA8820460E72E7600"))
# print()
#
# # CPU Static
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=2431, core_id=83, arch="x86-64", rated_speed=2.400)
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=1111, core_id=83, arch="x86-64", rated_speed=2.400)
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=3244, core_id=83, arch="x86-64", rated_speed=2.400)
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=1231, core_id=33, arch="x86-64", rated_speed=2.400)
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=8888, core_id=33, arch="x86-64", rated_speed=2.400)
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=9984, core_id=33, arch="x86-64", rated_speed=2.400)
# cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=2431, core_id=83, arch="UPDATED ARCH", rated_speed=2.400)
#
#
# cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=9222, core_id=92, arch="x86-64", rated_speed=3.800)
# cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=9684, core_id=92, arch="x86-64", rated_speed=3.800)
# cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=5701, core_id=44, arch="x86-64", rated_speed=3.800)
# cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=1923, core_id=44, arch="x86-64", rated_speed=3.800)
# cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=3341, core_id=74, arch="x86-64", rated_speed=3.800)
# print(cpuStatic.select_all_cpus_by_serial("NXGTSAA8820460E72E7600"))
# print(cpuStatic.select_all_cpus_by_serial("QXGTSAA0912872E72E7600"))
# print()
#
# # CPU Active
# cpuActive.insert_cpu_active(cpu_static_id=1, utilization=20.1, speed=24009.21)
# print(cpuActive.select_recent_cpu_active(cpu_static_id=1))
# cpuActive.insert_cpu_active(cpu_static_id=1, utilization=18.2, speed=23899.12)
# print(cpuActive.select_recent_cpu_active(cpu_static_id=1))
# cpuActive.insert_cpu_active(cpu_static_id=1, utilization=14.1, speed=25678.99)
# print(cpuActive.select_recent_cpu_active(cpu_static_id=1))
# print()
#
# # GPU Static
# gpuStatic.insert_gpu_static(serial="NXGTSAA8820460E72E7600", slot=1, gpu_type="discrete", manufacturer="Nvidia",
#                             kernel_module="nvidia-smi")
# gpuStatic.insert_gpu_static(serial="OXGTSAA7744859E72E7600", slot=1, gpu_type="integrated", manufacturer="Nvidia",
#                             kernel_module="nvidia-smi")
# gpuStatic.insert_gpu_static(serial="PXGTSAA0892634E72E7600", slot=1, gpu_type="integrated", manufacturer="Intel",
#                             kernel_module="intel-gpu-tools")
# gpuStatic.insert_gpu_static(serial="QXGTSAA0912872E72E7600", slot=1, gpu_type="discrete", manufacturer="AMD")
# gpuStatic.insert_gpu_static(serial="QXGTSAA0912872E72E7600", slot=2, gpu_type="discrete", manufacturer="AMD")
# # Reading From GPU Static
# print(gpuStatic.select_gpu_by_serial("NXGTSAA8820460E72E7600"))
# print(gpuStatic.select_gpu_by_serial("OXGTSAA7744859E72E7600"))
# print(gpuStatic.select_gpu_by_serial("PXGTSAA0892634E72E7600"))
# print(gpuStatic.select_gpu_by_serial("QXGTSAA0912872E72E7600"))
# # Update GPUStatic
# gpuStatic.insert_gpu_static(serial="NXGTSAA8820460E72E7600", slot=1, gpu_type="discrete", manufacturer="Intel",
#                             kernel_module="intel-gpu-tools")
# print(gpuStatic.select_gpu_by_serial("NXGTSAA8820460E72E7600"))
# print()
#
#
# # Disk Static
# diskStatic.insert_disk_static(serial="NXGTSAA8820460E72E7600", disk="SSD")
# diskStatic.insert_disk_static(serial="OXGTSAA7744859E72E7600", disk="HDD")
# diskStatic.insert_disk_static(serial="PXGTSAA0892634E72E7600", disk="SSD")
# diskStatic.insert_disk_static(serial="QXGTSAA0912872E72E7600", disk="HDD")
# diskStatic.insert_disk_static(serial="NXGTSAA8820460E72E7600", disk="SSD", make="UPDATED DISK")
#
# # Reading From Disk Static
# print(diskStatic.select_disk_by_serial("NXGTSAA8820460E72E7600"))
# print(diskStatic.select_disk_by_serial("OXGTSAA7744859E72E7600"))
# print(diskStatic.select_disk_by_serial("PXGTSAA0892634E72E7600"))
# print(diskStatic.select_disk_by_serial("QXGTSAA0912872E72E7600"))
# print()
#
# # Disk Active
# diskActive.insert_disk_active(disk_static_id=1, read=1234, write=2134)
# print(diskActive.select_recent_disk_active(disk_static_id=1))
# diskActive.insert_disk_active(disk_static_id=1, read=4321432, write=1242)
# print(diskActive.select_recent_disk_active(disk_static_id=1))
# diskActive.insert_disk_active(disk_static_id=1, read=4322, write=241241)
# print(diskActive.select_recent_disk_active(disk_static_id=1))
# diskActive.insert_disk_active(disk_static_id=1, read=4312423, write=42314)
# print(diskActive.select_recent_disk_active(disk_static_id=1))
# print()
#
# # Partition
# partition.insert_partition(disk_static_id=1, name="/dev/sda1", size_total=200000, size_used=120000, size_available=80000)
# partition.insert_partition(disk_static_id=1, name="/dev/sda2", size_total=10000, size_used=1000, size_available=9000)
# partition.insert_partition(disk_static_id=2, name="/dev/sda1", size_total=200000, size_used=120000, size_available=80000)
# print(partition.select_partition_by_disk(disk_static_id=1))
# print(partition.select_partition_by_disk(disk_static_id=2))
# partition.insert_partition(disk_static_id=1, name="/dev/sda1", size_total=200000, size_used=180000, size_available=20000)
# print(partition.select_partition_by_disk(disk_static_id=1))
# print()
#
# # Processes
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=231452, cpu_percent=22.1, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=432512, cpu_percent=21.1, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=214518, cpu_percent=2.1, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=4556, cpu_percent=9.4, mem_percent=1.0)
# print()
#
# # Network Static
# netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 9f c7 9a 07", ip="127.0.0.1")
# netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 76 c7 9a 43", ip="127.0.0.2")
# netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 11 c7 23 01", ip="127.0.0.3")
# netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 9f c7 9a 07", ip="127.0.0.1", mac="UPDATED NETWORK")
# print(netStatic.select_net_static(serial="NXGTSAA8820460E72E7600"))
# print()
#
# # Network Active
# netActive.insert_net_active(net_static_id=1, bytes_in=123451, bytes_out=564279)
# netActive.insert_net_active(net_static_id=1, bytes_in=896951, bytes_out=22)
# netActive.insert_net_active(net_static_id=1, bytes_in=432, bytes_out=4325111)
# netActive.insert_net_active(net_static_id=1, bytes_in=77899443, bytes_out=12399)
# netActive.insert_net_active(net_static_id=1, bytes_in=2, bytes_out=345)
# print()
#
# # Host Environment
# hostEnv.insert_host_env(serial="NXGTSAA8820460E72E7600", hostname="RH7")
# print(hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600"))
# hostEnv.insert_host_env(serial="NXGTSAA8820460E72E7600", hostname="RH7")
# print(hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600"))
# hostEnv.insert_host_env(serial="OXGTSAA7744859E72E7600", hostname="RH7")
# print(hostEnv.select_host_by_serial(serial="OXGTSAA7744859E72E7600"))
# hostEnv.insert_host_env(serial="NXGTSAA8820460E72E7600", hostname="RH8")
# print(hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600"))
#

# print("First Processes.")
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=1, cpu_percent=22.1, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=2, cpu_percent=21.1, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=3, cpu_percent=2.1, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=4, cpu_percent=9.4, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=5, cpu_percent=0, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=6, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=7, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=8, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=9, cpu_percent=8, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=10, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=11, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=12, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=13, cpu_percent=0, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=14, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=15, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=16, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=17, cpu_percent=0, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=18, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=19, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=20, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=21, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=22, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=23, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=24, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=25, cpu_percent=0, mem_percent=1.0)

# print()
# time.sleep(3.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=1, cpu_percent=22.1, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=2, cpu_percent=9.1, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=3, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=4, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=5, cpu_percent=0, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=6, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=7, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=8, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=9, cpu_percent=8, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=10, cpu_percent=25, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=11, cpu_percent=3, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=12, cpu_percent=5, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=13, cpu_percent=2, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=14, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=15, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=16, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=17, cpu_percent=0, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=18, cpu_percent=0, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=19, cpu_percent=0, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=20, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=21, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=22, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=23, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=24, cpu_percent=0, mem_percent=1.0)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=25, cpu_percent=0, mem_percent=1.0)
# print("Processes after 3.0 seconds.")