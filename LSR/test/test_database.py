from database import machineId, cpuStatic, cpuActive, User, uptime, partition, diskStatic, \
    diskActive, memStatic, memActive, netStatic, netActive, hostEnv, gpuStatic, userAccess
import datetime
from database import Database as database


# MACHINE IDENTIFICATION
def test_machine_select():
    # EMPTY, NO MACHINES
    machines = machineId.select_all_machines()
    assert machines is None


def test_machine_insert():
    machineId.insert_machine_id(serial="NXGTSAA8820460E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-1")
    machine = machineId.select_machine("NXGTSAA8820460E72E7600")
    print(machine)
    assert machine['Serial'] == "NXGTSAA8820460E72E7600"


def test_machine_insert2():
    machineId.insert_machine_id(serial="OXGTSAA7744859E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-2")
    machine = machineId.select_machine("OXGTSAA7744859E72E7600")
    print(machine)
    assert machine["Serial"] == "OXGTSAA7744859E72E7600"


def test_machine_insert3():
    machineId.insert_machine_id(serial="PXGTSAA0892634E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-3")
    machine = machineId.select_machine("PXGTSAA0892634E72E7600")
    print(machine)
    assert machine["Serial"] == "PXGTSAA0892634E72E7600"


def test_machine_insert4():
    machineId.insert_machine_id(serial="QXGTSAA0912872E72E7600", model="Aspire E5-576G", make="Acer", name="laptop-4")
    machine = machineId.select_machine("QXGTSAA0912872E72E7600")
    print(machine)
    assert machine["Serial"] == "QXGTSAA0912872E72E7600"


# CPU STATIC
def test_cpu_static_select():
    cpus = cpuStatic.select_all_cpus_by_serial(serial="NXGTSAA8820460E72E7600")
    assert cpus is None


def test_cpu_static1():
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=2431, core_id=83, arch="x86-64",
                                rated_speed=2.400)
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=1111, core_id=83, arch="x86-64",
                                rated_speed=2.400)
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=3244, core_id=83, arch="x86-64",
                                rated_speed=2.400)
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=1231, core_id=33, arch="x86-64",
                                rated_speed=2.400)
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=8888, core_id=33, arch="x86-64",
                                rated_speed=2.400)
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=9984, core_id=33, arch="x86-64",
                                rated_speed=2.400)

    cpus = cpuStatic.select_all_cpus_by_serial("NXGTSAA8820460E72E7600")
    print(cpus)

    assert len(cpus) != 0


def test_cpu_static2():
    cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=9222, core_id=92, arch="x86-64",
                                rated_speed=3.800)
    cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=9684, core_id=92, arch="x86-64",
                                rated_speed=3.800)
    cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=5701, core_id=44, arch="x86-64",
                                rated_speed=3.800)
    cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=1923, core_id=44, arch="x86-64",
                                rated_speed=3.800)
    cpuStatic.insert_cpu_static(serial="QXGTSAA0912872E72E7600", processor_id=3341, core_id=74, arch="x86-64",
                                rated_speed=3.800)

    cpus = cpuStatic.select_all_cpus_by_serial("QXGTSAA0912872E72E7600")
    print(cpus)
    assert len(cpus) != 0


def test_cpus_update():
    cpuStatic.insert_cpu_static(serial="NXGTSAA8820460E72E7600", processor_id=2431, core_id=83, arch="UPDATED ARCH",
                                rated_speed=2.400)
    cpus = cpuStatic.select_all_cpus_by_serial(serial="NXGTSAA8820460E72E7600")

    cpu_id = None
    for cpu in cpus:
        if cpu['Arch'] == "UPDATED ARCH":
            cpu_id = cpu['CPUStaticID']
            print(cpu)

    assert cpu_id is not None


# CPU ACTIVE
def test_cpu_active1():
    cpuActive.insert_cpu_active(cpu_static_id=1, utilization=20.1, speed=24009.21)
    active = cpuActive.select_recent_cpu_active(cpu_static_id=1)
    print(active)
    assert active is not None


def test_cpu_active2():
    cpuActive.insert_cpu_active(cpu_static_id=1, utilization=18.2, speed=23899.12)
    active = cpuActive.select_recent_cpu_active(cpu_static_id=1)
    print(active)
    assert active is not None


def test_cpu_active3():
    cpuActive.insert_cpu_active(cpu_static_id=1, utilization=14.1, speed=25678.99)
    active = cpuActive.select_recent_cpu_active(cpu_static_id=1)
    print(active)
    assert active is not None


# USER
def test_user1():
    user = User.User()
    user.insert_user(email="alex.fuoco1@marist.edu", username="alexfuoco", first_name="Alex", last_name="Fuoco",
                     is_admin=True, password_hash="fdsanfduianjker", salt="jkfdanjod")
    result = user.select_user_by_id(1)
    assert result is not None


def test_user2():
    user = User.User()
    user.insert_user(email="Louis.Kavouras1@marist.edu", username="loukavouras", first_name="Lou", last_name="Kavouras",
                     is_admin=True, password_hash="45254njkluhf948", salt="jkfdanjod")
    result = user.select_user_by_id(2)
    assert result is not None


def test_user3():
    user = User.User()
    user.insert_user(email="jason.boniello1@marist.edu", username="jasonboniello", first_name="Jason", last_name="Boniello",
                     is_admin=False, password_hash="nmkolijmnbhulkmi", salt="jkfdanjod")
    result = user.select_user_by_id(3)
    assert result is not None


def test_user4():
    # Already Inputted User
    user = User.User()
    user.insert_user(email="jason.boniello1@marist.edu", username="jasonboniello", first_name="Jason", last_name="Boniello",
                     is_admin=False, password_hash="nmkolijmnbhulkmi", salt="jkfdanjod")
    result = user.select_user_by_id(4)
    assert result is None


def test_user_select1():
    user = User.User()
    result = user.select_user_by_email("alex.fuoco1@marist.edu")
    print(result)
    assert result is not None


def test_user_select2():
    user = User.User()
    result = user.select_user_by_email("Louis.Kavouras1@marist.edu")
    print(result)
    assert result is not None


def test_user_select3():
    user = User.User()
    result = user.select_user_by_email("jason.boniello1@marist.edu")
    print(result)
    assert result is not None


def test_user_select_all():
    user = User.User()
    result = user.select_all_users()
    print(result)
    assert len(result) == 3


# USER ACCESS
def test_user_access():
    result = userAccess.select_user_access(1)
    assert result is None


def test_user_access1():
    userAccess.insert_user_access(user_id=1, serial="NXGTSAA8820460E72E7600")
    result = userAccess.select_user_access(1)
    print(result)
    assert len(result) == 1


def test_user_access2():
    userAccess.insert_user_access(user_id=1, serial="PXGTSAA0892634E72E7600")
    userAccess.insert_user_access(user_id=1, serial="QXGTSAA0912872E72E7600")
    result = userAccess.select_user_access(1)
    print(result)
    assert len(result) == 3


def test_user_access3():
    userAccess.insert_user_access(user_id=2, serial="NXGTSAA8820460E72E7600")
    userAccess.insert_user_access(user_id=2, serial="OXGTSAA7744859E72E7600")
    userAccess.insert_user_access(user_id=2, serial="PXGTSAA0892634E72E7600")
    userAccess.insert_user_access(user_id=2, serial="QXGTSAA0912872E72E7600")
    result = userAccess.select_user_access(2)
    print(result)
    assert len(result) == 4


def test_user_access4():
    userAccess.insert_user_access(user_id=3, serial="PXGTSAA0892634E72E7600")
    userAccess.insert_user_access(user_id=3, serial="QXGTSAA0912872E72E7600")
    result = userAccess.select_user_access(3)
    print(result)
    assert len(result) == 2


# HOST ENV
def test_host():
    result = hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600")
    assert result is None


def test_host1():
    hostEnv.insert_host_env(serial="NXGTSAA8820460E72E7600", hostname="RH7")
    result = hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


def test_host2():
    hostEnv.insert_host_env(serial="OXGTSAA7744859E72E7600", hostname="RH7")
    result = hostEnv.select_host_by_serial(serial="OXGTSAA7744859E72E7600")
    print(result)
    assert result is not None


def test_host_update():
    result1 = hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result1)
    hostEnv.insert_host_env(serial="NXGTSAA8820460E72E7600", hostname="RH8")
    result2 = hostEnv.select_host_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result2)
    assert result1['Hostname'] != result2['Hostname']


# UPTIME
def test_uptime1():
    uptime.insert_uptime(serial="QXGTSAA0912872E72E7600", uptime=datetime.datetime.now())
    result = uptime.select_uptime_by_serial(serial="QXGTSAA0912872E72E7600")
    print(result)
    assert result is not None


def test_uptime2():
    uptime.insert_uptime(serial="QXGTSAA0912872E72E7600", uptime=datetime.datetime.now()+datetime.timedelta(minutes=15))
    result = uptime.select_uptime_by_serial(serial="QXGTSAA0912872E72E7600")
    print(result)
    assert result is not None


def test_uptime3():
    uptime.insert_uptime(serial="QXGTSAA0912872E72E7600", uptime=datetime.datetime.now()+datetime.timedelta(minutes=30))
    result = uptime.select_uptime_by_serial(serial="QXGTSAA0912872E72E7600")
    print(result)
    assert result is not None


def test_uptime_select():
    result = uptime.select_uptime_by_serial(serial="QXGTSAA0912872E72E7600")
    print(result)
    assert result is not None


# DISK STATIC
def test_disk_stat1():
    diskStatic.insert_disk_static(serial="NXGTSAA8820460E72E7600", disk="SSD", size_available=7778345, size_total=19)
    result = diskStatic.select_disk_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


def test_disk_stat2():
    diskStatic.insert_disk_static(serial="OXGTSAA7744859E72E7600", disk="HDD", size_available=1234545, size_total=44)
    result = diskStatic.select_disk_by_serial(serial="OXGTSAA7744859E72E7600")
    print(result)
    assert result is not None


def test_disk_stat3():
    diskStatic.insert_disk_static(serial="PXGTSAA0892634E72E7600", disk="SSD", size_available=245566425, size_total=9999)
    result = diskStatic.select_disk_by_serial(serial="PXGTSAA0892634E72E7600")
    print(result)
    assert result is not None


def test_select_disk_stat1():
    result = diskStatic.select_disk_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


def test_select_disk_stat2():
    result = diskStatic.select_disk_by_serial(serial="OXGTSAA7744859E72E7600")
    print(result)
    assert result is not None


def test_select_disk_stat3():
    result = diskStatic.select_disk_by_serial(serial="PXGTSAA0892634E72E7600")
    print(result)
    assert result is not None


def test_update_disk_stat():
    diskStatic.insert_disk_static(serial="NXGTSAA8820460E72E7600", disk="SSD", make="UPDATED DISK")
    result = diskStatic.select_disk_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


def test_select_update_disk_stat():
    result = diskStatic.select_disk_by_serial(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


# Disk Active
def test_disk_act():
    result = diskActive.select_recent_disk_active(disk_static_id=1)
    assert result is None


def test_disk_act1():
    diskActive.insert_disk_active(disk_static_id=1, read=1234, write=2134)
    result = diskActive.select_recent_disk_active(disk_static_id=1)
    print(result)
    assert result is not None


def test_disk_act2():
    result1 = diskActive.select_recent_disk_active(disk_static_id=1)
    diskActive.insert_disk_active(disk_static_id=1, read=4321432, write=1242)
    result2 = diskActive.select_recent_disk_active(disk_static_id=1)
    print(result2)
    assert result1['Read'] != result2['Read']


def test_disk_act3():
    diskActive.insert_disk_active(disk_static_id=1, read=4322, write=241241)
    result1 = diskActive.select_recent_disk_active(disk_static_id=1)
    print(result1)
    assert result1 is not None


# PARTITION
def test_partition1():
    partition.insert_partition(disk_static_id=1, name="/dev/sda1", size_total=200000, size_used=120000,
                               size_available=80000)
    result = partition.select_partition_by_disk(disk_static_id=1)
    print(result)
    assert result is not None


def test_partition2():
    partition.insert_partition(disk_static_id=2, name="/dev/sda1", size_total=200000, size_used=120000,
                               size_available=80000)
    result = partition.select_partition_by_disk(disk_static_id=2)
    print(result)
    assert result is not None


def test_partition_select1():
    result = partition.select_partition_by_disk(disk_static_id=1)
    print(result)
    assert result is not None


def test_partition_select2():
    result = partition.select_partition_by_disk(disk_static_id=2)
    print(result)
    assert result is not None


def test_partition_update():
    partition.insert_partition(disk_static_id=1, name="/dev/sda1", size_total=200000, size_used=180000,
                               size_available=20000)
    result = partition.select_partition_by_disk(disk_static_id=1)
    print(result)
    assert result is not None


def test_partition_select3():
    result = partition.select_partition_by_disk(disk_static_id=1)
    print(result)
    assert result is not None


# MEMORY STATIC
def test_mem_stat1():
    memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=8,
                                speed=1600, make=None, model=None)
    result = memStatic.select_mem_static("NXGTSAA8820460E72E7600")
    print(result)
    assert len(result) == 1


def test_mem_stat2():
    memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM2', mem_type="SODIMM", unit_size=8,
                                speed=1600, make=None, model=None)
    result = memStatic.select_mem_static("NXGTSAA8820460E72E7600")
    print(result)
    assert len(result) == 2


def test_mem_stat_select1():
    result = memStatic.select_mem_static("NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


def test_mem_update():
    memStatic.insert_mem_static(serial="NXGTSAA8820460E72E7600", dimm_slot='CPU0 DIMM1', mem_type="SODIMM", unit_size=8,
                                speed=1600, make="UPDATED ROW", model=None)
    result = memStatic.select_mem_static("NXGTSAA8820460E72E7600")
    print(result[0]['Make'])
    assert result[0]['Make'] == "UPDATED ROW"


def test_mem_stat_select2():
    result = memStatic.select_mem_static("NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


# MEMORY ACTIVE
def test_mem_act():
    result = memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600")
    assert result is None


def test_mem_act1():
    memActive.insert_mem_active(serial="NXGTSAA8820460E72E7600", total_size=8000, free=1300, cache=1200, swap=None)
    result = memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert result is not None


def test_mem_act2():
    memActive.insert_mem_active(serial="NXGTSAA8820460E72E7600", total_size=8000, free=1100, cache=1400, swap=None)
    result1 = memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600")
    print(result1)
    memActive.insert_mem_active(serial="NXGTSAA8820460E72E7600", total_size=8000, free=1800, cache=1100, swap=None)
    result2 = memActive.select_recent_mem_active(serial="NXGTSAA8820460E72E7600")
    print(result2)
    assert result1['Free'] != result2['Free']


def test_mem_hist1():
    query1 = "INSERT INTO `MemoryActive`(`Serial`, `EntryDate`) VALUES('NXGTSAA8820460E72E7600', '2021-05-07 00:00:00')"
    query2 = "INSERT INTO `MemoryActive`(`Serial`, `EntryDate`) VALUES('NXGTSAA8820460E72E7600', '2021-05-07 00:00:01')"
    query3 = "INSERT INTO `MemoryActive`(`Serial`, `EntryDate`) VALUES('NXGTSAA8820460E72E7600', '2021-05-07 23:59:59')"
    query4 = "INSERT INTO `MemoryActive`(`Serial`, `EntryDate`) VALUES('NXGTSAA8820460E72E7600', '2021-05-08 00:00:00')"
    query5 = "INSERT INTO `MemoryActive`(`Serial`, `EntryDate`) VALUES('NXGTSAA8820460E72E7600', '2021-05-08 00:00:01')"

    conn = database.get_db_connection()
    cursor = conn.cursor(buffered=True)

    cursor.execute(query1)
    cursor.execute(query2)
    cursor.execute(query3)
    cursor.execute(query4)
    cursor.execute(query5)

    conn.commit()

    cursor.close()
    conn.close()
    results = memActive.select_by_day(serial="NXGTSAA8820460E72E7600", day=datetime.datetime.strptime("2021-05-07",
                                                                                                      "%Y-%m-%d"))
    print("MEMORY HISTORY RESULTS:")
    print(results)
    assert len(results) == 3


def test_mem_hist2():
    results = memActive.select_by_day(serial="NXGTSAA8820460E72E7600", day=datetime.datetime.strptime("2021-05-08",
                                                                                                      "%Y-%m-%d"))
    print("MEMORY HISTORY RESULTS:")
    print(results)
    assert len(results) == 2


# NETWORK STATIC
def test_net_stat():
    result = netStatic.select_net_static(serial="NXGTSAA8820460E72E7600")
    assert result is None


def test_net_stat1():
    netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600",
                                interface="b2 31 9f c7 9a 07",
                                ip="127.0.0.1")
    result = netStatic.select_net_static(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert len(result) == 1


def test_net_stat2():
    netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 76 c7 9a 43", ip="127.0.0.2")
    result = netStatic.select_net_static(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert len(result) == 2


def test_net_stat3():
    netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 11 c7 23 01", ip="127.0.0.3")
    result = netStatic.select_net_static(serial="NXGTSAA8820460E72E7600")
    print(result)
    assert len(result) == 3


def test_net_stat_update():
    netStatic.insert_net_static(serial="NXGTSAA8820460E72E7600", interface="b2 31 9f c7 9a 07", ip="127.0.0.1",
                                mac="UPDATED NETWORK")
    result = netStatic.select_net_static(serial="NXGTSAA8820460E72E7600")
    print(result[0]['Mac'])
    assert result[0]['Mac'] == "UPDATED NETWORK"


# NETWORK ACTIVE
def test_net_act():
    result = netActive.select_recent_net_active(net_static_id=1)
    assert result is None


def test_net_act1():
    netActive.insert_net_active(net_static_id=1, bytes_in=123451, bytes_out=564279)
    result = netActive.select_recent_net_active(net_static_id=1)
    print(result)
    assert result is not None


def test_net_act2():
    netActive.insert_net_active(net_static_id=1, bytes_in=896951, bytes_out=22)
    result1 = netActive.select_recent_net_active(net_static_id=1)
    print(result1)
    netActive.insert_net_active(net_static_id=1, bytes_in=432, bytes_out=4325111)
    result2 = netActive.select_recent_net_active(net_static_id=1)
    print(result2)
    assert result1 != result2


def test_net_act3():
    netActive.insert_net_active(net_static_id=1, bytes_in=2, bytes_out=345)
    result = netActive.select_recent_net_active(net_static_id=1)
    print(result)
    assert result is not None


# GPU Static
def test_gpu_stat():
    result = gpuStatic.select_gpu_by_serial("NXGTSAA8820460E72E7600")
    assert result is None


def test_gpu_stat1():
    gpuStatic.insert_gpu_static(serial="NXGTSAA8820460E72E7600",
                                slot=1,
                                gpu_type="discrete",
                                manufacturer="Nvidia",
                                kernel_module="nvidia-smi")
    result = gpuStatic.select_gpu_by_serial("NXGTSAA8820460E72E7600")
    print(result)
    assert len(result) == 1


def test_gpu_stat2():
    gpuStatic.insert_gpu_static(serial="OXGTSAA7744859E72E7600", slot=1, gpu_type="integrated", manufacturer="Nvidia",
                                kernel_module="nvidia-smi")
    result = gpuStatic.select_gpu_by_serial("OXGTSAA7744859E72E7600")
    print(result)
    assert len(result) == 1


def test_gpu_stat3():
    gpuStatic.insert_gpu_static(serial="PXGTSAA0892634E72E7600", slot=1, gpu_type="integrated", manufacturer="Intel",
                                kernel_module="intel-gpu-tools")
    result = gpuStatic.select_gpu_by_serial("PXGTSAA0892634E72E7600")
    print(result)
    assert len(result) == 1


def test_gpu_stat4():
    gpuStatic.insert_gpu_static(serial="QXGTSAA0912872E72E7600", slot=1, gpu_type="discrete", manufacturer="AMD")
    gpuStatic.insert_gpu_static(serial="QXGTSAA0912872E72E7600", slot=2, gpu_type="discrete", manufacturer="AMD")
    result = gpuStatic.select_gpu_by_serial("QXGTSAA0912872E72E7600")
    print(result)
    assert len(result) == 2


def test_gpu_stat_update():
    result1 = gpuStatic.select_gpu_by_serial("NXGTSAA8820460E72E7600")
    gpuStatic.insert_gpu_static(serial="NXGTSAA8820460E72E7600",
                                slot=1,
                                gpu_type="discrete",
                                manufacturer="Intel",
                                kernel_module="intel-gpu-tools")
    result2 = gpuStatic.select_gpu_by_serial("NXGTSAA8820460E72E7600")
    print(result1[0])
    print(result2[0])
    assert result1[0]['Slot'] == result2[0]['Slot']


# Processes
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=1, cpu_percent=22.1, mem_percent=1.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=2, cpu_percent=21.1, mem_percent=4.7)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=3, cpu_percent=2.1, mem_percent=0.2)
# processes.insert_processes(serial="NXGTSAA8820460E72E7600", pid=4, cpu_percent=9.4, mem_percent=1.0)
# print()
