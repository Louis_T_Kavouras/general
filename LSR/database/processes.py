from mysql.connector import errorcode
from database import Database as database 
import mysql.connector


def insert_processes(serial, pid, user=None, cpu_percent=None, mem_percent=None,
                     runtime=None, command=None):
    """ Inserting into the Disk Static table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        process_data = {
            'Serial': serial,
            'PID': pid,
            'User': user,
            'CPUPercent': cpu_percent,
            'MemoryPercent': mem_percent,
            'Runtime': runtime,
            'Command': command
        }

        add_process = ("INSERT INTO `Processes` "
                       "(`Serial`, `PID`, `User`, `CPUPercent`, `MemoryPercent`, "
                       "`Runtime`, `Command`) "
                       "VALUES (%(Serial)s, %(PID)s, %(User)s, %(CPUPercent)s, "
                       "%(MemoryPercent)s, %(Runtime)s, %(Command)s)")

        cursor.execute(add_process, process_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("Process inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_recent_processes(serial):
    """Selects 20 most recent from Processes by Serial"""
    conn = database.get_db_connection()

    proc_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `Processes` "
                 "WHERE `Serial` = %(Serial)s "
                 "ORDER BY `EntryDate` DESC LIMIT 20")

        cursor.execute(query, proc_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No current Processes for Serial: " + str(serial))
            formatted_result = None
        else:
            print("Found current processes")
            for row in result:
                formatted_result.append({
                    'ProcessesId': row[0],
                    'Serial': row[1],
                    'PID': row[2],
                    'User': row[3],
                    'CPUPercent': row[4],
                    'MemoryPercent': row[5],
                    'Runtime': row[6],
                    'Command': row[7],
                    'EntryDate': row[8]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result
