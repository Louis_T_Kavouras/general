from mysql.connector import errorcode
from database import Database
import datetime
import mysql.connector


def insert_mem_active(serial, total_size=None, free=None, cache=None, swap=None):
    """ Inserting into the Memory Active table"""

    conn = Database.get_db_connection()
    try:
        cursor = conn.cursor()

        mem_active_data = {
            'Serial': serial,
            'TotalSize': total_size,
            'Free': free,
            'Cache': cache,
            'Swap': swap,
        }

        add_mem_active = ("INSERT INTO `MemoryActive` "
                          "(`Serial`, `TotalSize`, `Free`, `Cache`, `Swap`) "
                          "VALUES (%(Serial)s, %(TotalSize)s, %(Free)s, %(Cache)s, %(Swap)s)")

        cursor.execute(add_mem_active, mem_active_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("MemActive inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_by_day(serial, start_date, end_date):
    """Selects from MemoryActive by Serial between a Start Date and an End Date"""
    conn = Database.get_db_connection()

    mem_active_data = {
        'Serial': serial,
        'StartDate': start_date,
        'EndDate': end_date
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `MemoryActive` "
                 "WHERE `Serial` = %(Serial)s "
                 "AND `EntryDate` BETWEEN %(StartDate)s AND %(EndDate)s")

        cursor.execute(query, mem_active_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No current Memory Active Data for Serial: " + str(serial) + " found.")
            formatted_result = None
        else:
            print("Found current Memory Active")
            for row in result:
                formatted_result.append({
                    'MemActiveID': row[0],
                    'Serial': row[1],
                    'TotalSize': row[2],
                    'Free': row[3],
                    'Cache': row[4],
                    'Swap': row[5],
                    'EntryDate': row[6]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_recent_mem_active(serial):
    """Selects most recent from MemoryActive by Serial"""
    conn = Database.get_db_connection()

    mem_active_data = {
        'Serial': serial,
        'Before': datetime.datetime.now() - datetime.timedelta(minutes=15),
        'Now': datetime.datetime.now()
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `MemoryActive` "
                      "WHERE `Serial` = %(Serial)s "
                      "AND `EntryDate` BETWEEN %(Before)s AND %(Now)s")

        cursor.execute(query_mach, mem_active_data)

        result = cursor.fetchall()
        if result is None or len(result) == 0:
            print("No current Memory Active Data for Serial: " + str(serial) + " found.")
            result = None
        else:
            print("Found current Memory Active")
            result = {
                'MemActiveID': result[-1][0],
                'Serial': result[-1][1],
                'TotalSize': result[-1][2],
                'Free': result[-1][3],
                'Cache': result[-1][4],
                'Swap': result[-1][5],
                'EntryDate': result[-1][6]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
