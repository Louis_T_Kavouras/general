from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_uptime(serial, uptime):
    """ Inserting into the Uptime table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        uptime_data = {
            'Serial': serial,
            'UptimeValue': uptime
        }

        add_uptime = ("INSERT INTO `Uptime` (`Serial`, `UptimeValue`) "
                      "VALUES (%(Serial)s, %(UptimeValue)s)")

        cursor.execute(add_uptime, uptime_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("Uptime inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_uptime_by_serial(serial):
    """
        Selects most recent from Serial by DiskStatic
        Selects all from the past 15 minutes and returns the most recent entry
    """
    conn = database.get_db_connection()

    uptime_data = {
        'Serial': serial,
        'Before': datetime.datetime.now() - datetime.timedelta(minutes=15),
        'Now': datetime.datetime.now()
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `Uptime` "
                 "WHERE `Serial` = %(Serial)s "
                 "AND `EntryDate` BETWEEN %(Before)s AND %(Now)s")

        cursor.execute(query, uptime_data)

        result = cursor.fetchall()
        if result is None or len(result) == 0:
            result = None
        else:
            print("Found most recent Uptime Data")
            result = {
                'UptimeID': result[-1][0],
                'Serial': result[-1][1],
                'UptimeValue': result[-1][2],
                'EntryDate': result[-1][3]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
