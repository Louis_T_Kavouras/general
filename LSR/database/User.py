from mysql.connector import errorcode
from database import Database as database
import mysql.connector


class User:
    def __init__(self, user_id="", email="", username="", lastname="",
                 firstname="", isadmin="", password="", salt="", phone="",
                 entrydate=""):
        self.user_id = user_id
        self.email = email
        self.username = username
        self.lastname = lastname
        self.firstname = firstname
        self.isadmin = isadmin
        self.password = password
        self.salt = salt
        self.phone = phone
        self.entrydate = entrydate

    def insert_user(self, email, password_hash, salt, username, is_admin, last_name=None,
                    first_name=None, phone=None):
        """"
            Inserting a user into the database
            All fields are required, phone, last name and first name  can be NULL
        """
        conn = database.get_db_connection()

        try:
            cursor = conn.cursor()

            user_data = {
                'Email': email,
                'UserName': username,
                'LastName': last_name,
                'FirstName': first_name,
                'isAdmin': is_admin,
                'PasswordHash': password_hash,
                'Salt': salt,
                'Phone': phone,
            }

            if self.select_user_by_email(email=email) is not None:
                invalid_entry = True  # User already inserted
            else:
                invalid_entry = False  # New User, can insert
                add_user = (
                    "INSERT INTO `User` (`Email`, `UserName`, `LastName`, `FirstName`, `isAdmin`, `PasswordHash`, "
                    "`Salt`, `Phone`) "
                    "VALUES (%(Email)s, %(UserName)s, %(LastName)s, %(FirstName)s, %(isAdmin)s, %(PasswordHash)s, "
                    "%(Salt)s, %(Phone)s)")
                cursor.execute(add_user, user_data)
                conn.commit()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print("Something is wrong with the table")
            elif err.errno == errorcode.ER_NO_SUCH_TABLE:
                print("Table does not exist")
            else:
                print(err)
        else:
            if invalid_entry is False:
                print("User inserted Successfully, connection closing")
            else:
                print("Invalid insert user: User already has account")
            cursor.close()
            conn.close()

    def select_all_users(self):
        """Selects all users"""
        conn = database.get_db_connection()

        try:
            cursor = conn.cursor(buffered=True)

            query_user = "SELECT * FROM `User`"

            cursor.execute(query_user)

            result = cursor.fetchall()
            formatted_result = []
            if result is None or len(result) == 0:
                print("No users found.")
                formatted_result = None
            else:
                print("Found users")
                for row in result:
                    formatted_result.append(
                        dict(UserId=row[0], Email=row[1], UserName=row[2], LastName=row[3],
                             FirstName=row[4], isAdmin=row[5], PasswordHash=row[6], Salt=row[7],
                             Phone=row[8], EntryDate=row[9]))

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print("Something is wrong with the table")
            elif err.errno == errorcode.ER_NO_SUCH_TABLE:
                print("Table does not exist")
            else:
                print(err)
        else:
            cursor.close()
            conn.close()
            return formatted_result

    def select_user_by_id(self, user_id):
        """Selects user by id"""
        conn = database.get_db_connection()

        user_data = {
            'userid': user_id
        }

        try:
            cursor = conn.cursor(buffered=True)

            query_user = ("SELECT * FROM `User` "
                          "WHERE `UserID` = %(userid)s")

            cursor.execute(query_user, user_data)

            result = cursor.fetchone()
            if result is None or len(result) == 0:
                print("No user with id: " + str(user_id) + " found.")
                result = None
            else:
                print("Found user")
                result = dict(UserId=result[0], Email=result[1], UserName=result[2], LastName=result[3],
                              FirstName=result[4], isAdmin=result[5], PasswordHash=result[6], Salt=result[7],
                              Phone=result[8], EntryDate=result[9])

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print("Something is wrong with the table")
            elif err.errno == errorcode.ER_NO_SUCH_TABLE:
                print("Table does not exist")
            else:
                print(err)
        else:
            cursor.close()
            conn.close()
            return result

    def select_user_by_email(self, email):
        """Selects user by email"""
        conn = database.get_db_connection()

        user_data = {
            'Email': email
        }

        try:
            cursor = conn.cursor(buffered=True)

            query_user = ("SELECT * FROM `User` "
                          "WHERE `Email` = %(Email)s")

            cursor.execute(query_user, user_data)

            result = cursor.fetchone()
            if result is None or len(result) == 0:
                print("No user with email: " + str(email) + " found.")
                result = None
            else:
                print("Found user")
                result = dict(UserId=result[0], Email=result[1], UserName=result[2], LastName=result[3],
                              FirstName=result[4], isAdmin=result[5], PasswordHash=result[6], Salt=result[7],
                              Phone=result[8], EntryDate=result[9])

        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_BAD_TABLE_ERROR:
                print("Something is wrong with the table")
            elif err.errno == errorcode.ER_NO_SUCH_TABLE:
                print("Table does not exist")
            else:
                print(err)
        else:
            cursor.close()
            conn.close()
            return result
