from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_mem_static(serial, dimm_slot, mem_type=None, unit_size=None, speed=None, make=None, model=None):
    """ Inserting into the Memory Static table"""

    conn = database.get_db_connection()
    try:
        cursor = conn.cursor()

        mem_static_data = {
            'Serial': serial,
            'DIMMSlot': dimm_slot,
            'Type': mem_type,
            'UnitSize': unit_size,
            'Speed': speed,
            'Make': make,
            'Model': model,
        }

        add_mem_stat = ("INSERT INTO `MemoryStatic` "
                        "(`Serial`, `DIMMSlot`, `Type`, `UnitSize`, `Speed`, `Make`, `Model`) "
                        "VALUES (%(Serial)s, %(DIMMSlot)s, %(Type)s, %(UnitSize)s, %(Speed)s, %(Make)s, %(Model)s)")

        prev_mem_static = check_serial_dimm(serial=serial, dimm_slot=dimm_slot)
        if prev_mem_static is not None:
            # Check to see if already in the database
            if prev_mem_static['DIMMSlot'] == dimm_slot:
                # Change from an insert to an update
                print("Updating MemStatic with Serial: " + str(serial) + "and DIMM: " + str(dimm_slot))
                mem_static_data['MemStaticID'] = prev_mem_static['MemStaticID']
                mem_static_data['EntryDate'] = datetime.datetime.now()

                add_mem_stat = ("UPDATE `MemoryStatic` SET `MemStaticID` = %(MemStaticID)s, "
                                "`Serial` = %(Serial)s, `DIMMSlot` = %(DIMMSlot)s, `Type` = %(Type)s, "
                                "`UnitSize` = %(UnitSize)s, `Speed` = %(Speed)s, `Make` = %(Make)s, "
                                "`Model` = %(Model)s, `EntryDate` = %(EntryDate)s "
                                "WHERE `MemStaticID` = %(MemStaticID)s")

        cursor.execute(add_mem_stat, mem_static_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("MemStatic inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def check_serial_dimm(serial, dimm_slot):
    """Selects one of MemoryStatic by Serial and DIMM"""

    conn = database.get_db_connection()

    mem_static_data = {
        'Serial': serial,
        "DIMMSlot": dimm_slot
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `MemoryStatic` "
                      "WHERE `Serial`=%(Serial)s AND `DIMMSlot`=%(DIMMSlot)s ")

        cursor.execute(query_mach, mem_static_data)

        result = cursor.fetchone()
        formatted_result = {}
        if result is None or len(result) == 0:
            # No memory in db
            formatted_result = None
        else:
            print("Found current MemoryStatic installed")
            formatted_result = {
                'MemStaticID': result[0],
                'Serial': result[1],
                'DIMMSlot': result[2],
                'Type': result[3],
                'UnitSize': result[4],
                'Speed': result[5],
                'Make': result[6],
                'Model': result[7],
                'EntryDate': result[8]
            }

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_mem_static(serial):
    """Selects all MemoryStatic by Serial"""
    conn = database.get_db_connection()

    mem_static_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `MemoryStatic` "
                      "WHERE `Serial` = %(Serial)s")

        cursor.execute(query_mach, mem_static_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No memory installed for: " + serial + ".")
            formatted_result = None
        else:
            print("Found current MemoryStatic installed")
            for row in result:
                formatted_result.append({
                    'MemStaticID': row[0],
                    'Serial': row[1],
                    'DIMMSlot': row[2],
                    'Type': row[3],
                    'UnitSize': row[4],
                    'Speed': row[5],
                    'Make': row[6],
                    'Model': row[7],
                    'EntryDate': row[8]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result
