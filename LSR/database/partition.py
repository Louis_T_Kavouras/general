from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_partition(disk_static_id, name, mount=None, size_available=None, size_total=None,
                     size_used=None, partition_type=None):
    """ Inserting into Partition table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        partition_data = {
            'DiskStaticID': disk_static_id,
            'Mount': mount,
            'Name': name,
            'SizeAvailable': size_available,
            'SizeTotal': size_total,
            'SizeUsed': size_used,
            'Type': partition_type
        }

        add_partition = ("INSERT INTO `Partition`"
                         "(`DiskStaticID`, `Mount`, `Name`, `SizeAvailable`, "
                         "`SizeTotal`, `SizeUsed`, `Type`) "
                         "VALUES (%(DiskStaticID)s, %(Mount)s, %(Name)s, %(SizeAvailable)s, "
                         "%(SizeTotal)s, %(SizeUsed)s, %(Type)s)")

        prev_partition = check_disk_name(disk_static_id=disk_static_id, name=name)
        if prev_partition is not None:
            # Check to see if already in the database
            if prev_partition['Name'] == name:
                # Change from an insert to an update
                print("Updating Partition with DiskStaticID: " + str(disk_static_id) +
                      " and Name: " + str(name))
                partition_data['PartitionID'] = prev_partition['PartitionID']
                partition_data['EntryDate'] = datetime.datetime.now()

                add_partition = ("UPDATE `Partition` SET `PartitionID` = %(PartitionID)s, "
                                 "`DiskStaticID` = %(DiskStaticID)s, `Mount` = %(Mount)s, `Name` = %(Name)s, "
                                 "`SizeAvailable` = %(SizeAvailable)s, `SizeTotal` = %(SizeTotal)s, "
                                 "`SizeUsed` = %(SizeUsed)s, `Type` = %(Type)s, `EntryDate`= %(EntryDate)s "
                                 "WHERE `PartitionID` = %(PartitionID)s")

        cursor.execute(add_partition, partition_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("Partition inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def check_disk_name(disk_static_id, name):
    """Selects one of Partition by DiskStaticID and Name"""

    conn = database.get_db_connection()

    partition_data = {
        'DiskStaticID': disk_static_id,
        'Name': name
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `Partition` "
                 "WHERE `DiskStaticID`=%(DiskStaticID)s AND `Name`=%(Name)s ")

        cursor.execute(query, partition_data)

        result = cursor.fetchone()
        formatted_result = {}
        if result is None or len(result) == 0:
            # No partition in db
            formatted_result = None
        else:
            print("Found current Partition")
            formatted_result = {
                'PartitionID': result[0],
                'DiskStaticID': result[1],
                'Name': result[2],
                'Type': result[3],
                'SizeTotal': result[4],
                'SizeUsed': result[5],
                'SizeAvailable': result[6],
                'Mount': result[7],
                'EntryDate': result[8]
            }

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_partition_by_disk(disk_static_id):
    """Selects all partitions by DiskStaticId"""
    conn = database.get_db_connection()

    partition_data = {
        'DiskStaticId': disk_static_id
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `Partition` "
                      "WHERE `DiskStaticId` = %(DiskStaticId)s")

        cursor.execute(query_mach, partition_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No partition for diskStatic: " + str(disk_static_id) + " found.")
            formatted_result = None
        else:
            print("Found current partition Data")
            for row in result:
                formatted_result.append({
                    'PartitionID': row[0],
                    'DiskStaticID': row[1],
                    'Name': row[2],
                    'Type': row[3],
                    'SizeTotal': row[4],
                    'SizeUsed': row[5],
                    'SizeAvailable': row[6],
                    'Mount': row[7],
                    'EntryDate': row[8]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result
