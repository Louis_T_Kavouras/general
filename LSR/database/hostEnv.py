from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_host_env(serial, hostname, bios_vers=None, bios_date=None,
                    os_release=None, os_kernel=None, os_arch=None):
    """ Inserting into the Host Environment table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        host_env_data = {
            'Serial': serial,
            'Hostname': hostname,
            'BiosVersion': bios_vers,
            'BiosDate': bios_date,
            'OSRelease': os_release,
            'OSKernel': os_kernel,
            'OSArch': os_arch
        }

        add_host = ("INSERT INTO `HostEnvironment` "
                    "(`Serial`, `Hostname`, `BiosVersion`, `BiosDate`, "
                    "`OSRelease`, `OSKernel`, `OSArch`) "
                    "VALUES (%(Serial)s, %(Hostname)s, %(BiosVersion)s, %(BiosDate)s, "
                    "%(OSRelease)s, %(OSKernel)s, %(OSArch)s)")

        prev_host = select_host_by_serial(serial=serial)
        # Check to see if already in the database
        if prev_host is not None:
            # Change from an insert to an update
            print("Updating Host with Serial: " + str(serial) + "and hostname: " + str(hostname))
            host_env_data['HostID'] = prev_host['HostID']
            host_env_data['EntryDate'] = datetime.datetime.now()

            add_host = ("UPDATE `HostEnvironment` SET `HostID` = %(HostID)s, "
                        "`Serial` = %(Serial)s, `Hostname` = %(Hostname)s, `BiosVersion` = %(BiosVersion)s, "
                        "`BiosDate` = %(BiosDate)s, `OSRelease` = %(OSRelease)s, `OSKernel` = %(OSKernel)s, "
                        "`OSArch` = %(OSArch)s, `EntryDate` = %(EntryDate)s "
                        "WHERE `HostID` = %(HostID)s")

        cursor.execute(add_host, host_env_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("HostEnv inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_host_by_serial(serial):
    """Selects all hosts by Serial"""
    conn = database.get_db_connection()

    host_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `HostEnvironment` "
                 "WHERE `Serial` = %(Serial)s")

        cursor.execute(query, host_data)

        result = cursor.fetchone()
        formatted_result = {}
        if result is None or len(result) == 0:
            formatted_result = None
        else:
            print("Found current Host Environment Data")
            formatted_result = {
                'HostID': result[0],
                'Serial': result[1],
                'Hostname': result[2],
                'BiosVersion': result[3],
                'BiosDate': result[4],
                'OSRelease': result[5],
                'OSKernel': result[6],
                'OSArch': result[7],
                'EntryDate': result[8]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result
