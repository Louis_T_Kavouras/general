from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_net_active(net_static_id, bytes_in=None, bytes_out=None):
    """ Inserting into the Network Active table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        net_active_data = {
            'NetStaticID': net_static_id,
            'BytesIn': bytes_in,
            'BytesOut': bytes_out,
        }

        add_net_static = ("INSERT INTO `NetworkActive` "
                          "(`NetStaticID`, `BytesIn`, `BytesOut`) "
                          "VALUES (%(NetStaticID)s, %(BytesIn)s, %(BytesOut)s)")

        cursor.execute(add_net_static, net_active_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("NetworkActive inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_by_day(net_static_id, start_date, end_date):
    """Selects from NetActive by NetStaticID between a Start Date and an End Date"""
    conn = database.get_db_connection()

    net_active_data = {
        'NetStaticID': net_static_id,
        'StartDate': start_date,
        'EndDate': end_date
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `NetworkActive` "
                 "WHERE `NetStaticID` = %(NetStaticID)s "
                 "AND `EntryDate` BETWEEN %(StartDate)s AND %(EndDate)s")

        cursor.execute(query, net_active_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No Net Active Data between the provided dates " +
                  "for NetStaticID: " + str(net_static_id) + " found.")
            formatted_result = None
        else:
            print("Found Network Active")
            for row in result:
                formatted_result.append(dict(NetActiveID=row[0], NetStaticID=row[1], BytesIn=row[2],
                                             BytesOut=row[3], EntryDate=row[4]))
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_recent_net_active(net_static_id):
    """Selects most recent from NetworkActive by NetStaticID"""
    conn = database.get_db_connection()

    net_active_data = {
        'NetStaticID': net_static_id,
        'Before': datetime.datetime.now() - datetime.timedelta(minutes=15),
        'Now': datetime.datetime.now()
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `NetworkActive` "
                 "WHERE `NetStaticID` = %(NetStaticID)s "
                 "AND `EntryDate` BETWEEN %(Before)s AND %(Now)s")

        cursor.execute(query, net_active_data)

        result = cursor.fetchall()
        if result is None or len(result) == 0:
            print("No current Net Active Data with NetStaticID: " + str(net_static_id))
            result = None
        else:
            print("Found current Network Active")
            result = {
                'NetActiveID': result[-1][0],
                'NetStaticID': result[-1][1],
                'BytesIn': result[-1][2],
                'BytesOut': result[-1][3],
                'EntryDate': result[-1][4]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
