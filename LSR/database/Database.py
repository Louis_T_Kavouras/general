from mysql.connector import errorcode
import mysql.connector


def get_db_connection():
    """Establish and return the MySQL database connection"""
    try:
        db_connection = mysql.connector.connect(
            host="localhost",
            user="root",
            password="password",
            #password="MySQLD3v3l0pm3nt",
            #database="lsr"
            database="LSR"
        )
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
        return err
    else:
        # connection successful
        return db_connection
