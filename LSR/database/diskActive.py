from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_disk_active(disk_static_id, read=None, write=None):
    """ Inserting into the Disk Active table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        disk_active_data = {
            'DiskStaticID': disk_static_id,
            'Read': read,
            'Write': write,
        }

        add_disk_active = ("INSERT INTO `DiskActive`"
                           "(`DiskStaticID`, `Read`, `Write`) "
                           "VALUES (%(DiskStaticID)s, %(Read)s, %(Write)s)")

        cursor.execute(add_disk_active, disk_active_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("DiskActive inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_by_day(disk_static_id, start_date, end_date):
    """Selects from DiskActive by DiskStaticID between a Start Date and an End Date"""
    conn = database.get_db_connection()

    disk_active_data = {
        'DiskStaticID': disk_static_id,
        'StartDate': start_date,
        'EndDate': end_date
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `DiskActive` "
                 "WHERE `DiskStaticID` = %(DiskStaticID)s "
                 "AND `EntryDate` BETWEEN %(StartDate)s AND %(EndDate)s")

        cursor.execute(query, disk_active_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No current Disk Active Data between the provided dates "
                  "for DiskStaticID: " + str(disk_static_id) + " found.")
            formatted_result = None
        else:
            print("Found current DiskActive")
            for row in result:
                formatted_result.append({
                    'DiskActiveID': row[0],
                    'DiskStaticID': row[1],
                    'Read': row[2],
                    'Write': row[3],
                    'EntryDate': row[4]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_recent_disk_active(disk_static_id):
    """
    Selects most recent from DiskActive by DiskStatic
    Selects all from the past 15 minutes and returns the most recent entry
    """
    conn = database.get_db_connection()

    disk_active_data = {
        'DiskStaticID': disk_static_id,
        'Before': datetime.datetime.now() - datetime.timedelta(minutes=15),
        'Now': datetime.datetime.now()
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `DiskActive` "
                      "WHERE `DiskStaticID` = %(DiskStaticID)s "
                      "AND `EntryDate` BETWEEN %(Before)s AND %(Now)s")

        cursor.execute(query_mach, disk_active_data)

        result = cursor.fetchall()
        if result is None or len(result) == 0:
            print("No current Disk Active Data for DiskStatic: " + str(disk_static_id) + " found.")
            result = None
        else:
            print("Found current Disk Active")
            result = {
                'DiskActiveID': result[-1][0],
                'DiskStaticID': result[-1][1],
                'Read': result[-1][2],
                'Write': result[-1][3],
                'EntryDate': result[-1][4]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
