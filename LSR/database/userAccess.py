from mysql.connector import errorcode
from database import Database 
import mysql.connector


def insert_user_access(user_id, serial):
    """Inserting into the UserAccess table, no null values allowed"""
    conn = Database.get_db_connection()

    try:
        cursor = conn.cursor()

        user_access_data = {
            'UserId': user_id,
            'Serial': serial,
        }

        add_user_access = ("INSERT INTO `UserAccess`"
                           "(`UserID`,`Serial`) "
                           "VALUES (%(UserId)s, %(Serial)s)")

        cursor.execute(add_user_access, user_access_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("UserAccess inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_user_access(user_id):
    """Selects from UserAccess by id"""
    conn = Database.get_db_connection()

    ua_data = {
        'userid': user_id
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_ua = ("SELECT * FROM `UserAccess` "
                    "WHERE `UserID` = %(userid)s")

        cursor.execute(query_ua, ua_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No workstations assigned to user with id number: " + str(user_id))
            formatted_result = None
        else:
            print("Found workstations user has access to")
            for row in result:
                formatted_result.append({
                    'UserId': row[0],
                    'Serial': row[1],
                    'EntryDate': row[2]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result
