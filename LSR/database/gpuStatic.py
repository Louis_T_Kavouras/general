from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_gpu_static(serial, slot, gpu_type=None, manufacturer=None, kernel_module=None):
    """ Inserting into the GPU Static table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        gpu_static_data = {
            'Serial': serial,
            'Slot': slot,
            'Type': gpu_type,
            'Manufacturer': manufacturer,
            'KernelModule': kernel_module,
        }

        add_gpu_static = ("INSERT INTO `GPUStatic`"
                          "(`Serial`, `Slot`, `Type`, `Manufacturer`, `KernelModule`) "
                          "VALUES (%(Serial)s, %(Slot)s, %(Type)s, %(Manufacturer)s, %(KernelModule)s)")

        prev_gpu_static = check_serial_slot(serial=serial, slot=slot)
        if prev_gpu_static is not None:
            # Check to see if already in the database
            if prev_gpu_static['Slot'] == slot:
                # Change from an insert to an update
                print("Updating GPUStatic with Serial: " + str(serial) + "and slot: " + str(slot))
                gpu_static_data['GPUStaticID'] = prev_gpu_static['GPUStaticID']
                gpu_static_data['EntryDate'] = datetime.datetime.now()

                add_gpu_static = ("UPDATE `GPUStatic` SET `GPUStaticID` = %(GPUStaticID)s, "
                                  "`Serial` = %(Serial)s, `Slot` = %(Slot)s, `Type` = %(Type)s, "
                                  "`Manufacturer` = %(Manufacturer)s, `KernelModule` = %(KernelModule)s,"
                                  "`EntryDate` = %(EntryDate)s "
                                  "WHERE `GPUStaticID` = %(GPUStaticID)s")

        cursor.execute(add_gpu_static, gpu_static_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("GPUStatic inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def check_serial_slot(serial, slot):
    """Selects GPU by Serial and Slot"""
    conn = database.get_db_connection()

    gpu_static_data = {
        'Serial': serial,
        'Slot': slot
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `GPUStatic` "
                 "WHERE `Serial` = %(Serial)s AND `Slot` = %(Slot)s")

        cursor.execute(query, gpu_static_data)

        result = cursor.fetchone()
        if result is None or len(result) == 0:
            result = None
        else:
            print("Found current GPUStatic Data")
            result = {
                'GPUStaticID': result[0],
                'Serial': result[1],
                'Slot': result[2],
                'Type': result[3],
                'Manufacturer': result[4],
                'KernelModule': result[5],
                'EntryDate': result[6]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result


def select_gpu_by_serial(serial):
    """Selects all GPUs by Serial"""
    conn = database.get_db_connection()

    gpu_static_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `GPUStatic` "
                      "WHERE `Serial` = %(Serial)s")

        cursor.execute(query_mach, gpu_static_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No GPUs installed for Serial: " + serial + ".")
            formatted_result = None
        else:
            print("Found current GPUStatic Data")
            for row in result:
                formatted_result.append({
                    'GPUStaticID': row[0],
                    'Serial': row[1],
                    'Slot': row[2],
                    'Type': row[3],
                    'Manufacturer': row[4],
                    'KernelModule': row[5],
                    'EntryDate': row[6]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_gpu(gpu_static_id):
    """Selects GPU by Id"""
    conn = database.get_db_connection()

    gpu_static_data = {
        'GPUStaticID': gpu_static_id
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `GPUStatic` "
                      "WHERE `GPUStaticID` = %(GPUStaticID)s")

        cursor.execute(query_mach, gpu_static_data)

        result = cursor.fetchone()
        if result is None or len(result) == 0:
            print("No GPUs with id: " + gpu_static_id + " found.")
            result = None
        else:
            print("Found current GPUStatic Data")
            result = dict(GPUStaticID=result[0], Serial=result[1], Slot=result[2], Type=result[3],
                          Manufacturer=result[4], KernelModule=result[5], EntryDate=result[6])
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
