from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_disk_static(serial, disk, make=None, model=None, size_available=None, size_total=None,):
    """ Inserting into the Disk Static table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        disk_static_data = {
            'Serial': serial,
            'Disk': disk,
            'Make': make,
            'Model': model,
            'SizeAvailable': size_available,
            'SizeTotal': size_total
        }

        add_disk_static = ("INSERT INTO `DiskStatic` "
                           "(`Serial`, `Disk`, `Make`, `Model`, `SizeAvailable`, `SizeTotal`) "
                           "VALUES (%(Serial)s, %(Disk)s, %(Make)s, %(Model)s, %(SizeAvailable)s, %(SizeTotal)s)")

        prev_disk_static = check_serial_disk(serial=serial, disk=disk)
        if prev_disk_static is not None:
            # Check to see if already in the database
            if prev_disk_static['Disk'] == disk:
                # Change from an insert to an update
                print("Updating DiskStatic with Serial: " + str(serial) + " and Disk: " + str(disk))
                disk_static_data['DiskStaticID'] = prev_disk_static['DiskStaticID']
                disk_static_data['EntryDate'] = datetime.datetime.now()

                add_disk_static = ("UPDATE `DiskStatic` SET `DiskStaticID` = %(DiskStaticID)s, "
                                   "`Serial` = %(Serial)s, `Disk` = %(Disk)s, "
                                   "`Make` = %(Make)s, `Model` = %(Model)s, "
                                   "`SizeAvailable` = %(SizeAvailable)s, `SizeTotal` = %(SizeTotal)s, "
                                   "`EntryDate` = %(EntryDate)s "
                                   "WHERE `DiskStaticID` = %(DiskStaticID)s")

        cursor.execute(add_disk_static, disk_static_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("DiskStatic inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def check_serial_disk(serial, disk):
    """Selects one of DiskStatic by Serial and Disk"""

    conn = database.get_db_connection()

    disk_static_data = {
        'Serial': serial,
        "Disk": disk
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `DiskStatic` "
                      "WHERE `Serial`=%(Serial)s AND `Disk`=%(Disk)s ")

        cursor.execute(query_mach, disk_static_data)

        result = cursor.fetchone()
        formatted_result = {}
        if result is None or len(result) == 0:
            # No disk static in db
            formatted_result = None
        else:
            print("Found current DiskStatic")
            formatted_result = {
                'DiskStaticID': result[0],
                'Serial': result[1],
                'Disk': result[2],
                'Make': result[3],
                'Model': result[4],
                'SizeAvailable': result[5],
                'SizeTotal': result[6],
                'EntryDate': result[7]
            }

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_disk_by_serial(serial):
    """Selects all disk by Serial"""
    conn = database.get_db_connection()

    disk_static_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `DiskStatic` "
                      "WHERE `Serial` = %(Serial)s")

        cursor.execute(query_mach, disk_static_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No Disks for Serial: " + serial + " found.")
            formatted_result = None
        else:
            print("Found current diskStatic Data")
            for row in result:
                formatted_result.append({
                    'DiskStaticID': row[0],
                    'Serial': row[1],
                    'Disk': row[2],
                    'Make': row[3],
                    'Model': row[4],
                    'SizeAvailable': row[5],
                    'SizeTotal': row[6],
                    'EntryDate': row[7]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_disk_by_id(disk_static_id):
    """Selects Disk by Id"""
    conn = database.get_db_connection()

    disk_static_data = {
        'DiskStaticID': disk_static_id
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `DiskStatic` "
                      "WHERE `DiskStaticID` = %(DiskStaticID)s")

        cursor.execute(query_mach, disk_static_data)

        result = cursor.fetchone()
        if result is None or len(result) == 0:
            print("No Disks with id: " + disk_static_id + " found.")
            result = None
        else:
            print("Found current DiskStatic Data")
            result = {
                'DiskStaticID': result[0],
                'Serial': result[1],
                'Disk': result[2],
                'Make': result[3],
                'Model': result[4],
                'SizeAvailable': result[5],
                'SizeTotal': result[6],
                'EntryDate': result[7]
            }

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
