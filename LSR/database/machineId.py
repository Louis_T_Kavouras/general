from mysql.connector import errorcode
import database.Database as Database
import mysql.connector


def insert_machine_id(serial, make=None, model=None, name=None):
    """
        Inserting into the MachineIdentification table,
        Only serial is required, all other fields can be null
    """
    conn = Database.get_db_connection()

    try:
        cursor = conn.cursor()

        mach_data = {
            'Serial': serial,
            'Make': make,
            'Model': model,
            'Name': name
        }

        add_mach_id = ("INSERT INTO `MachineIdentification`"
                       "(`Serial`, `Make`, `Model`, `Name`) "
                       "VALUES (%(Serial)s, %(Make)s, %(Model)s, %(Name)s)")

        cursor.execute(add_mach_id, mach_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("MachineIdentification inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_all_machines():
    """Selects all Machines """
    conn = Database.get_db_connection()

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `MachineIdentification` ")

        cursor.execute(query_mach)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No machines found.")
            formatted_result = None
        else:
            print("Found machine")
            for machine in result:
                formatted_result.append({'Serial': machine[0],
                                         'Make': machine[1],
                                         'Model': machine[2],
                                         'Name': machine[3],
                                         'EntryDate': machine[4]
                                         })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_machine(serial):
    """Selects Machine by Serial"""
    conn = Database.get_db_connection()

    mach_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `MachineIdentification` "
                      "WHERE `Serial` = %(Serial)s")

        cursor.execute(query_mach, mach_data)

        result = cursor.fetchone()
        if result is None or len(result) == 0:
            print("No machines with serial: " + serial + " found.")
            result = None
        else:
            print("Found machine")
            result = {'Serial': result[0],
                      'Make': result[1],
                      'Model': result[2],
                      'Name': result[3],
                      'EntryDate': result[4]
                      }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
