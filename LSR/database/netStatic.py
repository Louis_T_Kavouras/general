from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_net_static(serial, interface, ip=None, mac=None):
    """ Inserting into the Network Static table """

    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        network_data = {
            'Serial': serial,
            'Interface': interface,
            'IP': ip,
            'Mac': mac,
        }

        add_net_static = ("INSERT INTO `NetworkStatic` "
                          "(`Serial`, `Interface`, `IP`, `Mac`) "
                          "VALUES (%(Serial)s, %(Interface)s, %(IP)s, %(Mac)s)")

        prev_net_static = check_serial_interface(serial=serial, interface=interface)
        if prev_net_static is not None:
            # Check to see if already in the database
            if prev_net_static['Interface'] == interface:
                # Change from an insert to an update
                print("Updating NetworkStatic with Serial: " + str(serial) + " and Interface: " + str(interface))
                network_data['NetStaticID'] = prev_net_static['NetStaticID']
                network_data['EntryDate'] = datetime.datetime.now()

                add_net_static = ("UPDATE `NetworkStatic` SET `NetStaticID` = %(NetStaticID)s, "
                                  "`Serial` = %(Serial)s, `Interface` = %(Interface)s, "
                                  "`IP` = %(IP)s, `Mac` = %(Mac)s, `EntryDate` = %(EntryDate)s "
                                  "WHERE `NetStaticID` = %(NetStaticID)s")

        cursor.execute(add_net_static, network_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("NetworkStatic inserted Successfully, connection closing")
    finally:
        cursor.close()
        conn.close()


def check_serial_interface(serial, interface):
    """Selects all NetworkStatic by Serial and Interface"""
    conn = database.get_db_connection()

    net_static_data = {
        'Serial': serial,
        'Interface': interface
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `NetworkStatic` "
                 "WHERE `Serial` = %(Serial)s AND `Interface` = %(Interface)s")

        cursor.execute(query, net_static_data)

        result = cursor.fetchone()
        formatted_result = []
        if result is None or len(result) == 0:
            formatted_result = None
        else:
            print("Found current NetworkStatic installed")
            formatted_result = {
                'NetStaticID': result[0],
                'Serial': result[1],
                'Interface': result[2],
                'IP': result[3],
                'Mac': result[4],
                'EntryDate': result[5],
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_net_static(serial):
    """Selects all NetworkStatic by Serial"""
    conn = database.get_db_connection()

    net_static_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `NetworkStatic` "
                      "WHERE `Serial` = %(Serial)s")

        cursor.execute(query_mach, net_static_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No network data for: " + str(serial) + ".")
            formatted_result = None
        else:
            print("Found current NetworkStatic installed")
            for row in result:
                formatted_result.append({
                    'NetStaticID': row[0],
                    'Serial': row[1],
                    'Interface': row[2],
                    'IP': row[3],
                    'Mac': row[4],
                    'EntryDate': row[5],
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result
