from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_cpu_static(serial, processor_id, core_id, arch, rated_speed, make=None, model=None):
    """
        Inserting into the CPUStatic table,
        Serial, processorID and coreID is required, other fields are default null
    """
    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        cpu_static_data = {
            'Serial': serial,
            'ProcessorID': processor_id,
            'CoreID': core_id,
            'Arch': arch,
            'RatedSpeedMHz': rated_speed,
            'Make': make,
            'Model': model
        }

        add_cpu_static = ("INSERT INTO `CPUStatic`"
                          "(`Serial`, `ProcessorID`, `CoreID`, `Arch`, "
                          "`RatedSpeedMHz`, `Make`, `Model`) "
                          "VALUES (%(Serial)s, %(ProcessorID)s, %(CoreID)s, %(Arch)s, "
                          "%(RatedSpeedMHz)s, %(Make)s, %(Model)s)")

        prev_cpu_static = check_serial_processor(serial=serial, processor_id=processor_id)
        if prev_cpu_static is not None:
            # Check to see if already in the database
            if prev_cpu_static['ProcessorID'] == processor_id:
                # Change from an insert to an update
                print("Updating CPUStatic with Serial: " + str(serial) + " and ProcessorId: " + str(processor_id))
                cpu_static_data['CPUStaticID'] = prev_cpu_static['CPUStaticID']
                cpu_static_data['EntryDate'] = datetime.datetime.now()

                add_cpu_static = ("UPDATE `CPUStatic` SET `CPUStaticID` = %(CPUStaticID)s, "
                                  "`Serial` = %(Serial)s, `ProcessorID` = %(ProcessorID)s, `CoreID` = %(CoreID)s, "
                                  "`Arch` = %(Arch)s, `RatedSpeedMHz` = %(RatedSpeedMHz)s, `Make` = %(Make)s, "
                                  "`Model` = %(Model)s, `EntryDate` = %(EntryDate)s "
                                  "WHERE `CPUStaticID` = %(CPUStaticID)s")

        cursor.execute(add_cpu_static, cpu_static_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("Inserted Successfully into CPUStatic, connection closing")
    finally:
        cursor.close()
        conn.close()


def check_serial_processor(serial, processor_id):
    """Selects one of CPUStatic by Serial and DIMM"""

    conn = database.get_db_connection()

    cpu_static_data = {
        'Serial': serial,
        "ProcessorID": processor_id
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `CPUStatic` "
                      "WHERE `Serial`=%(Serial)s AND `ProcessorID`=%(ProcessorID)s ")

        cursor.execute(query_mach, cpu_static_data)

        result = cursor.fetchone()
        formatted_result = {}
        if result is None or len(result) == 0:
            # No memory in db
            formatted_result = None
        else:
            print("Found current CPUStatic installed")
            formatted_result = {
                'CPUStaticID': result[0],
                'Serial': result[1],
                'ProcessorID': result[2],
                'CoreID': result[3],
                'Arch': result[4],
                'RatedSpeedMHz': result[5],
                'Make': result[6],
                'Model': result[7],
                'EntryDate': result[8]
            }

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_all_cpus_by_serial(serial):
    """Selects all cpus by serial"""
    conn = database.get_db_connection()

    cpu_static_data = {
        'Serial': serial
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `CPUStatic` "
                      "WHERE `Serial` = %(Serial)s")

        cursor.execute(query_mach, cpu_static_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No CPUS for Serial: " + serial + " found.")
            formatted_result = None
        else:
            print("Found current cpuStatic Data")
            for row in result:
                formatted_result.append({
                    'CPUStaticID': row[0],
                    'Serial': row[1],
                    'ProcessorID': row[2],
                    'CoreID': row[3],
                    'Arch': row[4],
                    'RatedSpeedMHz': row[5],
                    'Make': row[6],
                    'Model': row[7],
                    'EntryDate': row[8]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_cpu(cpu_static_id):
    """Selects all cpu by CPUStaticId"""
    conn = database.get_db_connection()

    cpu_static_data = {
        'CPUStaticID': cpu_static_id
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `CPUStatic` "
                      "WHERE `CPUStaticID` = %(CPUStaticID)s")

        cursor.execute(query_mach, cpu_static_data)

        result = cursor.fetchone()
        if result is None or len(result) == 0:
            print("No CPU with id: " + str(cpu_static_id) + " found.")
            result = None
        else:
            print("Found current cpuStatic Data")
            result = dict(CPUStaticID=result[0], Serial=result[1], ProcessorID=result[2], CoreID=result[3],
                          Arch=result[4], RatedSpeedMHz=result[5], Make=result[6], Model=result[7], EntryDate=result[8])
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
