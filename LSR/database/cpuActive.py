from mysql.connector import errorcode
from database import Database as database
import datetime
import mysql.connector


def insert_cpu_active(cpu_static_id, utilization, speed):
    """
        Inserting into the CPUActive table,
        Only serial is required, all other fields can be null
    """
    conn = database.get_db_connection()

    try:
        cursor = conn.cursor()

        cpu_active_data = {
            'CPUStaticID': cpu_static_id,
            'SpeedMHz': speed,
            'Utilization': utilization,
        }

        add_cpu_active = ("INSERT INTO `CPUActive`"
                          "(`CPUStaticID`, `SpeedMHz`, `Utilization`) "
                          "VALUES (%(CPUStaticID)s, %(SpeedMHz)s, %(Utilization)s)")

        cursor.execute(add_cpu_active, cpu_active_data)
        conn.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
        # reverting changes because of exception
        conn.rollback()
    else:
        print("Inserted Successfully into CPUActive, connection closing")
    finally:
        cursor.close()
        conn.close()


def select_by_day(cpu_static_id, start_date, end_date):
    """Selects from CPUActive by CPUStaticID between a Start Date and an End Date"""
    conn = database.get_db_connection()

    cpu_active_data = {
        'CPUStaticID': cpu_static_id,
        'StartDate': start_date,
        'EndDate': end_date
    }

    try:
        cursor = conn.cursor(buffered=True)

        query = ("SELECT * FROM `CPUActive` "
                 "WHERE `CPUStaticID` = %(CPUStaticID)s "
                 "AND `EntryDate` BETWEEN %(StartDate)s AND %(EndDate)s")

        cursor.execute(query, cpu_active_data)

        result = cursor.fetchall()
        formatted_result = []
        if result is None or len(result) == 0:
            print("No CPUActive Data between the provided dates " +
                  "for CPUStaticID: " + str(cpu_static_id) + " found.")
            formatted_result = None
        else:
            print("Found CPU Active ")
            for row in result:
                formatted_result.append({
                    'CPUActiveID': row[0],
                    'CPUStaticID': row[1],
                    'SpeedMHz': row[2],
                    'Utilization': row[3],
                    'EntryDate': row[4]
                })
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return formatted_result


def select_recent_cpu_active(cpu_static_id):
    """Selects most recent from CPUActive by CPUStaticId"""
    conn = database.get_db_connection()

    cpu_active_data = {
        'CPUStaticID': cpu_static_id,
        'Before': datetime.datetime.now() - datetime.timedelta(minutes=15),
        'Now': datetime.datetime.now()
    }

    try:
        cursor = conn.cursor(buffered=True)

        query_mach = ("SELECT * FROM `CPUActive` "
                      "WHERE `CPUStaticID` = %(CPUStaticID)s "
                      "AND `EntryDate` BETWEEN %(Before)s AND %(Now)s")

        cursor.execute(query_mach, cpu_active_data)

        result = cursor.fetchall()
        if result is None or len(result) == 0:
            print("No current cpuActive for CPU: " + str(cpu_static_id) + " found.")
            result = None
        else:
            print("Found current cpuActiveData")
            result = {
                'CPUActiveID': result[-1][0],
                'CPUStaticID': result[-1][1],
                'SpeedMHz': result[-1][2],
                'Utilization': result[-1][3],
                'EntryDate': result[-1][4]
            }
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_BAD_TABLE_ERROR:
            print("Something is wrong with the table")
        elif err.errno == errorcode.ER_NO_SUCH_TABLE:
            print("Table does not exist")
        else:
            print(err)
    else:
        cursor.close()
        conn.close()
        return result
