CREATE database LSR;
use LSR;

CREATE TABLE `MachineIdentification` (
	`Serial` varchar(40) NOT NULL,
    `Make` varchar(40) DEFAULT NULL,
    `Model` varchar(20) DEFAULT NULL,
    `Name` varchar(40) DEFAULT NULL,
    `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `CPUStatic` (
  `CPUStaticID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `ProcessorID` int NOT NULL,
  `CoreID` mediumint NOT NULL,
  `Arch` varchar(20) DEFAULT NULL,
  `RatedSpeedMHz` decimal(8,3) DEFAULT NULL,
  `Make` varchar(40) DEFAULT NULL,
  `Model` varchar(50) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CPUStaticID`),
  KEY `CPUStatic_MachineIdentification_FK` (`Serial`),
  CONSTRAINT `CPUStatic_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `CPUActive` (
  `CPUActiveID` int NOT NULL AUTO_INCREMENT,
  `CPUStaticID` int NOT NULL,
  `SpeedMHz` decimal(8,3) DEFAULT NULL,
  `Utilization` decimal(4,1) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CPUActiveID`),
  KEY `CPUActive_CPUStatic_FK` (`CPUStaticID`),
  CONSTRAINT `CPUActive_CPUStatic_FK` FOREIGN KEY (`CPUStaticID`) REFERENCES `CPUStatic` (`CPUStaticID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `DiskStatic` (
  `DiskStaticID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `Disk` varchar(20) NOT NULL,
  `Make` varchar(40) DEFAULT NULL,
  `Model` varchar(50) DEFAULT NULL,
  `SizeTotal` decimal(15,0) DEFAULT NULL,
  `SizeAvailable` decimal(15,0) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`DiskStaticID`),
  KEY `CPUStatic_MachineIdentification_FK` (`Serial`) USING BTREE,
  CONSTRAINT `CPUStatic_MachineIdentification_FK_copy_copy` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `DiskActive` (
  `DiskActiveID` int NOT NULL AUTO_INCREMENT,
  `DiskStaticID` int NOT NULL,
  `Read` decimal(15,0) DEFAULT NULL,
  `Write` decimal(15,0) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`DiskActiveID`),
  KEY `DiskActive_DiskStatic_FK` (`DiskStaticID`),
  CONSTRAINT `DiskActive_DiskStatic_FK` FOREIGN KEY (`DiskStaticID`) REFERENCES `DiskStatic` (`DiskStaticID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `GPUStatic` (
  `GPUStaticID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `Slot` int NOT NULL,
  `Type` varchar(100) DEFAULT NULL,
  `Manufacturer` varchar(50) DEFAULT NULL,
  `KernelModule` varchar(40) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`GPUStaticID`),
  KEY `GPUStatic_MachineIdentification_FK` (`Serial`),
  CONSTRAINT `GPUStatic_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `HostEnvironment` (
  `HostID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `Hostname` varchar(50) NOT NULL,
  `BiosVersion` varchar(40) DEFAULT NULL,
  `BiosDate` date DEFAULT NULL,
  `OSRelease` varchar(100) DEFAULT NULL,
  `OSKernel` varchar(40) DEFAULT NULL,
  `OSArch` varchar(20) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`HostID`),
  KEY `HostEnvironment_MachineIdentification_FK` (`Serial`),
  CONSTRAINT `HostEnvironment_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `MemoryStatic` (
  `MemStaticID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `DIMMSlot` varchar(40) NOT NULL,
  `Type` varchar(20) DEFAULT NULL,
  `UnitSize` int DEFAULT NULL,
  `Speed` decimal(5,0) DEFAULT NULL,
  `Make` varchar(40) DEFAULT NULL,
  `Model` varchar(50) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MemStaticID`),
  KEY `CPUStatic_MachineIdentification_FK` (`Serial`) USING BTREE,
  CONSTRAINT `CPUStatic_MachineIdentification_FK_copy` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `MemoryActive` (
  `MemActiveID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `TotalSize` int DEFAULT NULL,
  `Free` int DEFAULT NULL,
  `Cache` int DEFAULT NULL,
  `Swap` int DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`MemActiveID`),
  KEY `CPUActive_MachineIdentification_FK` (`Serial`) USING BTREE,
  CONSTRAINT `CPUActive_MachineIdentification_FK_copy` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `NetworkStatic` (
  `NetStaticID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `Interface` varchar(20) NOT NULL,
  `IP` varchar(40) DEFAULT NULL,
  `Mac` varchar(50) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetStaticID`),
  KEY `NetworkStatic_MachineIdentification_FK` (`Serial`),
  CONSTRAINT `NetworkStatic_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `NetworkActive` (
  `NetActiveID` int NOT NULL AUTO_INCREMENT,
  `NetStaticID` int NOT NULL,
  `BytesIn` decimal(15,0) DEFAULT NULL,
  `BytesOut` decimal(10,0) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NetActiveID`),
  KEY `NetworkActive_NetworkStatic_FK` (`NetStaticID`),
  CONSTRAINT `NetworkActive_NetworkStatic_FK` FOREIGN KEY (`NetStaticID`) REFERENCES `NetworkStatic` (`NetStaticID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `Partition` (
  `PartitionID` int NOT NULL AUTO_INCREMENT,
  `DiskStaticID` int NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `SizeTotal` decimal(15,0) DEFAULT NULL,
  `SizeUsed` decimal(15,0) DEFAULT NULL,
  `SizeAvailable` decimal(15,0) DEFAULT NULL,
  `Mount` varchar(50) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PartitionID`),
  KEY `Partitions_DiskStatic_FK` (`DiskStaticID`),
  CONSTRAINT `Partitions_DiskStatic_FK` FOREIGN KEY (`DiskStaticID`) REFERENCES `DiskStatic` (`DiskStaticID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `Processes` (
  `ProcessesID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `PID` int NOT NULL,
  `User` varchar(50) DEFAULT NULL,
  `CPUPercent` decimal(4,1) DEFAULT NULL,
  `MemoryPercent` decimal(4,1) DEFAULT NULL,
  `Runtime` time DEFAULT NULL,
  `Command` varchar(50) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProcessesID`),
  CONSTRAINT `Processes_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `Uptime` (
  `UptimeID` int NOT NULL AUTO_INCREMENT,
  `Serial` varchar(40) NOT NULL,
  `UptimeValue` datetime NOT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UptimeID`),
  CONSTRAINT `Uptime_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `User` (
  `UserID` int NOT NULL AUTO_INCREMENT,
  `Email` varchar(100) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `LastName` varchar(40) DEFAULT NULL,
  `FirstName` varchar(40) DEFAULT NULL,
  `isAdmin` boolean NOT NULL DEFAULT FALSE,
  `PasswordHash` BLOB NOT NULL,
  `Salt` BLOB NOT NULL,
  `Phone` varchar(100) DEFAULT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `UserAccess` (
  `UserID` int NOT NULL,
  `Serial` varchar(40) NOT NULL,
  `EntryDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserID`,`Serial`),
  KEY `UserAccess_MachineIdentification_FK` (`Serial`),
  CONSTRAINT `UserAccess_MachineIdentification_FK` FOREIGN KEY (`Serial`) REFERENCES `MachineIdentification` (`Serial`),
  CONSTRAINT `UserAccess_User_FK` FOREIGN KEY (`UserID`) REFERENCES `User` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
