# Linux System Reporting (LSR) #
The Linux System Reporting tool (LSR), a software system designed to gather, log,
and display data pertaining to multiple client workstations and their resources. 

* Gathers various system metrics for reporting
* Aggregates metrics and stores in relational database
* Displays machines, metrics and visualizations via browser
---

## Installation ##
For installing the project clone the repository.

* Install the dependencies through pip for both [Daemon](https://bitbucket.org/Louis_T_Kavouras/general/src/master/daemon) and [LSR](https://bitbucket.org/Louis_T_Kavouras/general/src/master/LSR)
* Configure the WebLink.py file
	* Insert the external IP address or hostname of the webserver
	* This is the http://<IP.Address> the website is hosted on
* Configure the DataLink.py file
	* Insert the IP and Port of the configured database.
	* Typically point to localhost 127.0.0.1, port 3306
* Configure the [Database.py](https://bitbucket.org/Louis_T_Kavouras/general/src/master/LSR/database/Database.py) file
	* Update the host, user, and password
---

## LSR Initalization ##

* First, configure the MySQL database.
	* Run the [LSR_create_tables.sql](https://bitbucket.org/Louis_T_Kavouras/general/src/master/SQL/LSR_create_tables.sql) file for creating the LSR database.

* Second, start the server.
	```
	python3 lsr.py
	```	
---

## Daemon Initialization ##
* Start the LSR Daemon
	```
	python3 LsrDaemon.py
	```
---
### Group Members: ###
- Lou Kavouras
- Jason Boniello
- Alex Fuoco


### License: ###
LSR is [MIT Licensed](https://bitbucket.org/Louis_T_Kavouras/general/src/master/LICENSE).

	
### Version: ###
* 1.0.0
